/* 
 * File:   project.h
 * Author: Martin
 *
 * Created on 5 de marzo de 2019, 17:09
 */

#ifndef PROJECT_H
#define	PROJECT_H

#if defined(PIC_12F1840)
#include <12F1840.h>
#elif defined(PIC_16F1825)
#include <16F1825.h>

#bit TXCKSEL = getenv("bit:TXCKSEL");
#endif

#include "fuses.h"
#include "port.h"

#use delay(clock=32000000)

//#define RF_RX_TIMER0		//para que cuente con el timer0
//#define RF_MANTENIDO_TIME_OUT	1000	//cuanto tiempo tiene que pasar para que se interprete como que no esta mantenido (en mS)
//#define RF_DECODE_V1

//constantes
#define HEADER1			0xFF
#define HEADER2			0x55

#define ENCENDIDO		TRUE
#define APAGADO			FALSE

#define TX_BUFF_LEN		13
#define RX_BUFF_LEN		7

//config comunicacion
//#use rs232(baud=115200, UART1)
#use rs232(baud=250000, xmit=P_TX_TTL, rcv=P_RX_TTL, DISABLE_INTS)

int1 flagRecepcionOK = FALSE;
int16 VueltasTimer2 = 0;
int32 TiempoEntreRecepciones = 0;

char RxBuff[RX_BUFF_LEN];

#include "rf_library/rf_remotes.h"
#include "rf_library/rf_rx.c"
#include "rf_library/rf_tx.c"

rfRemote RecibAnteriorAnterior;
rfRemote RecibAnterior;	//direccion rf recibida
rfRemote Recibido;		//direccion rf recibida
rfRemote Transmision;	//direccion RF a enviar

#include "funciones.c"
#include "interrupts.c"

#endif	/* PROJECT_H */

