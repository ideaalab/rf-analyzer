/* 
 * File:   port.h
 * Author: Martin
 *
 * Created on 5 de marzo de 2019, 17:11
 */

#ifndef PORT_H
#define	PORT_H

#use fast_io(a)

#byte PORTA	= getenv("SFR:PORTA")

#define TRIS_A		0b11001110
#define WPU_A		0b00001000

#bit TX_TTL			= PORTA.0
#bit RX_TTL			= PORTA.1
#bit RX_RF			= PORTA.2
#bit MCLR			= PORTA.3
#bit RF_TX			= PORTA.4
#bit LED			= PORTA.5

#define P_TX_TTL	PIN_A0
#define P_RX_TTL	PIN_A1
#define P_RF_RX		PIN_A2
#define P_MCLR		PIN_A3
#define P_RF_TX		PIN_A4
#define P_LED		PIN_A5

#endif	/* PORT_H */

