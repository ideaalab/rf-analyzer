#include "project.h"

void main() {
	config();

	do{
		
		/* envia por SERIAL la informacion recibida por RF */
		if(DataReady() == TRUE){	//comprueba si la trama esta completa
			RFMantenido = TRUE;
			
			TiempoEntreRecepciones = ((int32)VueltasTimer2 * 256) + get_timer2();
			VueltasTimer2 = 0;
			set_timer2(0);
			
			//RecibAnteriorAnterior.Completo = RecibAnterior.Completo;
			//RecibAnterior.Completo = Recibido.Completo;
			Recibido.Completo = rfBuffer.Completo;
				
			//if((RecibAnteriorAnterior.Completo == RecibAnterior.Completo) && (RecibAnterior.Completo == Recibido.Completo)){
			//if(RecibAnterior.Completo == Recibido.Completo){
				RF2TTL();
			//}
		}
		
		/* envia por RF la informacion recibida por SERIAL */
		if(flagRecepcionOK == TRUE){
			flagRecepcionOK = FALSE;
			TTL2RF();
		}
		
		LED = RFmantenido;
	}while(TRUE);
}