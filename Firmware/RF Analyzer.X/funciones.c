void config(void);
void RF2TTL(void);
void TTL2RF(void);

void config(void){
	setup_wdt(WDT_OFF);						//configuracion wdt
	//setup_timer_0(T0_INTERNAL|T0_DIV_128);	//overflow cada 1.024mS
	//setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);	//usado para RF
	setup_timer_2(T2_DIV_BY_4,255,2);		//
	setup_dac(DAC_OFF);						//configura DAC
	setup_adc(ADC_OFF);						//configura ADC
	setup_ccp1(CCP_OFF);
	setup_spi(FALSE);						//configura SPI
	//setup_comparator(NC_NC);				//comparador apagado
	setup_vref(FALSE);						//no se usa voltaje de referencia
	setup_uart(TRUE);
	
	enable_interrupts(INT_TIMER2);			//se encarga de los time out de pulsacion y respuesta COM
	enable_interrupts(INT_RDA);

	setup_oscillator(OSC_8MHZ|OSC_PLL_ON);	//configura oscilador interno
	port_a_pullups(WPU_A);
	set_tris_a(TRIS_A);
	
	EncenderRF();		//inicializo RF IN
	
	LED = APAGADO;
}

void RF2TTL(void){
char TxBuff[TX_BUFF_LEN];

	//ApagarRF();
	
	int32 DataFrameTime = GetRFTime();
	int32 TER = TiempoEntreRecepciones - DataFrameTime;
	
	//comienzo de cadena
	TxBuff[0] = HEADER1;			//cabecera
	TxBuff[1] = HEADER2;			//cabecera

	//envio recibido por RF
	TxBuff[2] = Recibido.Bytes.Hi;	//byte high
	TxBuff[3] = Recibido.Bytes.Mi;	//byte mid
	TxBuff[4] = Recibido.Bytes.Lo;	//byte low

	//envio duracion de los pulsos
	TxBuff[5] = DataFrameTime>>24;	//hi
	TxBuff[6] = DataFrameTime>>16;	//mi hi
	TxBuff[7] = DataFrameTime>>8;	//mi lo
	TxBuff[8] = DataFrameTime;		//lo

	//envio tiempo entre recepciones
	TxBuff[9] = TER>>24;			//hi
	TxBuff[10] = TER>>16;			//mi hi
	TxBuff[11] = TER>>8;			//mi lo
	TxBuff[12] = TER;				//lo
	
	for(int8 x = 0; x < TX_BUFF_LEN; x++){
		putc(TxBuff[x]);
	}

	//EncenderRF();
}

void TTL2RF(void){
int PulseDuration, NumPulses;
	
	LED = ENCENDIDO;
	ApagarRF();
	
	Transmision.Bytes.Hi = RxBuff[2];
	Transmision.Bytes.Mi = RxBuff[3];
	Transmision.Bytes.Lo = RxBuff[4];
	
	PulseDuration = RxBuff[5];
	NumPulses = RxBuff[6];
	
	if(PulseDuration == 16){
		TX_16mS(Transmision, NumPulses);
	}
	else if(PulseDuration == 32){
		TX_32mS(Transmision, NumPulses);
	}
	else if(PulseDuration == 48){
		TX_48mS(Transmision, NumPulses);
	}
	else if(PulseDuration == 64){
		TX_64mS(Transmision, NumPulses);
	}
	
	EncenderRF();
	LED = APAGADO;
}