#include <12F1840.h>

//#FUSES NOFCMEN		//NO fail safe clock monitor
//#FUSES NOIESO		//NO internal external switchover
//#FUSES BROWNOUT		//brownout reset
//#FUSES NOCPD		//eeprom NO protegido de lecturas
//#FUSES NOPROTECT	//codigo NO protegido de lecturas
#FUSES MCLR			//mclr
//#FUSES NOPUT		//power up timer
//#FUSES NOWDT		//NO watch dog timer
#FUSES INTRC_IO		//oscilador interno, pines CLK como I/O

#use delay(clock=32000000)
#use fast_io(a)

#byte PORTA	= getenv("SFR:PORTA")

#define TRIS_A		0b11011110
#define PULL_UP_A	0b00001000

#define TX_TTL	PIN_A0
#define RX_TTL	PIN_A1
#bit RX_RF		= PORTA.2
//#bit MCLR		= PORTA.3
//#define X		PIN_A4
#bit LED		= PORTA.5

//#define RF_TIMER0		//para que cuente con el timer0
#define RF_COUNT_TIME	//para que muestre la duracion de la trama de datos

//posiciones de EEPROM
#define EE_MODO		0
#define EE_SIMPLE	1

//simbolos (configurar teniendo en cuenta la fuente usada por el programa de puerto serie)
#define SYM_HI	'H'	//simbolo para high
#define SYM_HL	'-'	//simbolo para flotando
#define SYM_LH	'\\'//simbolo para flotando invertido (solo mandos learning)
#define SYM_LO	'L'	//simbolo para low
#define SYM_PU	'X'	//simbolo para pulsado
#define SYM_NP	'�'	//simbolo para NO pulsado

//constantes
#define T_T2			10		//tiempo de overflow de timer 2
#define TMP_ANTIREBOTE	100		//milisegundos de antirebote para RF
#define PULSADO			FALSE
#define ENCENDIDO		TRUE
#define APAGADO			FALSE
#define TIME_OUT_PULSADORES	(TMP_ANTIREBOTE / T_T2)

//config comunicacion con iPod
#use rs232(baud=19200, UART1)

//short flagPulsacion = FALSE;	//mientras se esta recibiendo datos por RF se mantiene en TRUE
//short Mantenido = FALSE;		//indica si se esta manteniendo un pulsador.
short flagCaracterRecibido = FALSE;	//indica si se recibio un caracter por el puerto serial
short Simple = TRUE;			//modo simple muestra datos escenciales, modo complejo muestra bits y esas cosas
char RcvChar = 0;
char Modo = '4';

#include "rf_remotes.c"
#include "rf_rx.c"

//int ContadorPulsadorTimeOut = 0;	//contador de vueltas de timer 2 para distinguir cuando se deja de pulsar un boton

rfBuff RecibAnterior;	//direccion rf recibida
rfBuff Recibido;	//direccion rf recibida


#int_RDA
void UartInt(void){
	while(kbhit()){			//si hay algo pendiente de recibir
		RcvChar = getc();	//lo descargo
		flagCaracterRecibido = TRUE;
	}
}

/*#int_TIMER0
void timer0_isr(void){
//overflow cada 1,024mS

}

#int_TIMER1
void timer1_isr(void){
	
}

#int_TIMER2
void timer2_isr(void){
//si se parpadea el led quiere decir que no hay mandos sincronizados.
//si no hay mandos no se puede estar esperando un comando ni se
//ha pulsado un boton.

//si se ejecuta el timer significa que no se recibieron datos
//durante el tiempo requerido para overflow

		
	//compruebo si sigue mantenido el boton del mando
	if(Mantenido == TRUE){
		if(ContadorPulsadorTimeOut++ == TIME_OUT_PULSADORES){	

			ContadorPulsadorTimeOut = 0;
			flagPulsacion = FALSE;
			LED = FALSE;
			Mantenido = FALSE;
			RecibAnterior = 0;
			Recibido.Completo = 0;
			rfBuffer.Completo = 0;
			CountedBits = 0;
		}
	}
}*/

//Rutinas

void ShowBIN(int32 val, int8 num, int8 space, short head);	//envia por puerto serie un valor en formato binario
void ShowTRIN(int32 val, int8 num, int8 space, short head);	//envia por puerto serie un valor en formato trinario
void DecodeData(void);					//decodifica los datos y los convierte en comandos
//void SimularPulsacion(void);			//simula una pulsacion y el timer2 cuenta el tiempo que se mantiene pulsado
//void ActivarPulsadorTimeOut(void);	//reinicia contador para el timeout de los pulsadores
void AnalizarChar(void);
void LeerMem(void);

void main() {
	setup_wdt(WDT_OFF);						//configuracion wdt
	//setup_timer_0(T0_INTERNAL|T0_DIV_128);	//overflow cada 1.024mS
	//setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);	//usado para RF
	//setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);
	//setup_timer_2(T2_DIV_BY_64,250,5);		//overflow cada 10mS
	setup_dac(DAC_OFF);						//configura DAC
	setup_adc(ADC_OFF);						//configura ADC
	setup_ccp1(CCP_OFF);
	setup_spi(FALSE);						//configura SPI
	setup_comparator(NC_NC);				//comparador apagado
	setup_vref(FALSE);						//no se usa voltaje de referencia
	setup_uart(TRUE);
	
	//enable_interrupts(INT_TIMER2);	//se encarga de los time out de pulsacion y respuesta COM
	enable_interrupts(INT_RDA);

	setup_oscillator(OSC_8MHZ|OSC_PLL_ON);	//configura oscilador interno
	port_a_pullups(PULL_UP_A);
	set_tris_a(TRIS_A);
	
	LED = ENCENDIDO;
	
	LeerMem();
	delay_ms(500);
	/*printf("   \r\n");
	printf("\r\n--- Analizador RF ---\r\n");
	printf("Para ayuda usar el comando: ?\r\n\r\n");
	printf("Decodificador configurado para mando de %cCH\n\r", Modo);
	printf("Modo Simple: ");
	if(Simple == TRUE) printf("ACTIVADO"); else printf("DESACTIVADO");
	printf("\n\r\n\r");*/
	
	LED = APAGADO;
	
	enable_interrupts(GLOBAL);

	rf_in_init();		//inicializo RF IN

	do{
		if(DataReady() == TRUE){	//comprueba si la trama esta completa
			RecibAnterior.Completo = Recibido.Completo;
			Recibido.Completo = rfBuffer.Completo;
				
			//if(RecibAnterior.Completo == Recibido.Completo){
				LED = ENCENDIDO;
				
				/*#IFDEF RF_COUNT_TIME
				if(Simple == FALSE){	//muestra duracion de la trama de datos
					printf("Tiempo total: %Lu uS | 24bits de %Lu uS c/u | Sync de %Lu uS\r\n", TotalTime, TotalTime/32, TotalTime/8);
				}
				#ENDIF*/
				
				//DecodeData();	//convierte la trama de datos en pulsaciones
				
				//envio comienzo de cadena
				putc(0xFF);
				putc(0x55);
				
				//envio recibido por RF
				putc(Recibido.Bytes.Hi);	//btn
				putc(Recibido.Bytes.Mi);	//addr hi
				putc(Recibido.Bytes.Lo);	//addr lo
				
				//envio duracion de los pulsos
				putc(TotalTime>>24);	//hi
				putc(TotalTime>>16);	//mi hi
				putc(TotalTime>>8);		//mi lo
				putc(TotalTime);		//lo
				
				LED = APAGADO;
			//}
		}
		
		if(flagCaracterRecibido == TRUE){
			flagCaracterRecibido = FALSE;
			AnalizarChar();
		}
	}while(TRUE);
}

void DecodeData(void){
long Addr6CH = make16(Recibido.Ch6.AddrHi, Recibido.Ch6.AddrLo);
long Dat6Ch = (long)Recibido.Ch6.DatHi<<4 | Recibido.Ch6.DatLo;

	disable_interrupts(GLOBAL);

	//muestro recibido raw
	if(Simple == FALSE){
		printf("Recibido: ");
		ShowBIN(Recibido.Completo, 24, 8, TRUE);
		printf("  ( 0x%2X %2X %2X )\r\n", Recibido.Bytes.Hi, Recibido.Bytes.Mi, Recibido.Bytes.Lo);
	}
	
	switch(Modo){
		case('4'):
			//muestro addr
			if(Simple == TRUE) printf("ADDR ("); else printf("ADDR: ");
			ShowTRIN(Recibido.Ch4.Addr, 16, 16, FALSE);			//datos trinarios
			
			if(Simple == FALSE){
				printf("  |  ");
				ShowBIN(Recibido.Ch4.Addr, 16, 8, TRUE);			//datos binarios
				printf("  ( 0x%4LX )", Recibido.Ch4.Addr);	//datos exadecimales
			}
			
			if(Simple == FALSE) printf("\r\n");
			
			//muestro botones
			if(Simple == TRUE){
				printf(" ) | BTN ( D3:");
				if(Recibido.Ch4.D3 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" D2:");
				if(Recibido.Ch4.D2 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" D1:");
				if(Recibido.Ch4.D1 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" D0:");
				if(Recibido.Ch4.D0 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" )");
			}
			else{
				printf("BTN: ");
				ShowBIN(Recibido.Ch4.Dat, 8, 2, TRUE);
				printf("  ( 0x%2X )", Recibido.Ch4.Dat);	//datos exadecimales
			}
			printf("\r\n---------------------\r\n");
			if(Simple == FALSE) printf("\r\n");
			break;
			
		case('6'):
			//muestro addr
			if(Simple == TRUE) printf("ADDR ("); else printf("ADDR: ");
			ShowTRIN(Addr6Ch, 12, 12, FALSE);			//datos trinarios
			
			if(Simple == FALSE){
				printf("  |  ");
				ShowBIN(Addr6Ch, 12, 8, TRUE);			//datos binarios
				printf("  ( 0x%3LX )", Addr6Ch);	//datos exadecimales
			}
			
			if(Simple == FALSE) printf("\r\n");
			
			//muestro botones
			if(Simple == TRUE){
				printf(" ) | BTN ( D5:");
				if(Recibido.Ch6.D5 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" D4:");
				if(Recibido.Ch6.D4 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" D3:");
				if(Recibido.Ch6.D3 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" D2:");
				if(Recibido.Ch6.D2 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" D1:");
				if(Recibido.Ch6.D1 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" D0:");
				if(Recibido.Ch6.D0 == 0b11) putc(SYM_PU); else putc(SYM_NP);
				printf(" )");
			}
			else{
				printf("BTN: ");
				ShowBIN(Dat6Ch, 12, 2, TRUE);
				printf("  ( 0x%3LX )", Dat6Ch);	//datos exadecimales
			}
			printf("\r\n---------------------\r\n");
			if(Simple == FALSE) printf("\r\n");
			break;
			
		case('8'):
			//muestro addr
			if(Simple == TRUE) printf("ADDR ("); else printf("ADDR: ");
			ShowTRIN(Recibido.Ch8.Addr, 16, 16, FALSE);			//datos trinarios
			
			if(Simple == FALSE){
				printf("  |  ");
				ShowBIN(Recibido.Ch8.Addr, 16, 8, TRUE);			//datos binarios
				printf("  ( 0x%4LX )", Recibido.Ch8.Addr);	//datos exadecimales
			}
			
			if(Simple == FALSE) printf("\r\n");
			
			//muestro botones
			if(Simple == TRUE){
				printf(" ) | BTN ");
				switch(Recibido.Ch8.Dat){
					case BTN_8CH_1:
						putc('1'); break;
						
					case BTN_8CH_2:
						putc('2'); break;
						
					case BTN_8CH_3:
						putc('3'); break;
						
					case BTN_8CH_4:
						putc('4'); break;
					
					case BTN_8CH_5:
						putc('5'); break;
						
					case BTN_8CH_6:
						putc('6'); break;
						
					case BTN_8CH_7:
						putc('7'); break;
						
					case BTN_8CH_8:
						putc('8'); break;
					
					default:
						printf(" no reconocido\r\n");
				}
				
			}
			else{
				printf("BTN: ");
				ShowBIN(Recibido.Ch8.Dat, 8, 2, TRUE);
				printf("  ( 0x%2X )", Recibido.Ch8.Dat);	//datos exadecimales
			}
			printf("\r\n---------------------\r\n");
			if(Simple == FALSE) printf("\r\n");
			break;
			break;
			
		
	}
	enable_interrupts(GLOBAL);
}

void AnalizarChar(void){

	switch(RcvChar){
		case '4':	//mando 4ch
			Modo = RcvChar;
			write_eeprom(EE_MODO, Modo);
			//printf("\n\rDecodificador configurado para mando de 4CH\n\r\n\r");
			break;
			
		case '6':	//mando 6ch
			Modo = RcvChar;
			write_eeprom(EE_MODO, Modo);
			//printf("\n\rDecodificador configurado para mando de 6CH\n\r\n\r");
			break;
			
		case '8':	//mando 8ch
			Modo = RcvChar;
			write_eeprom(EE_MODO, Modo);
			//printf("\n\rDecodificador configurado para mando de 8CH\n\r\n\r");
			break;
		
		case 's':	//cambia el modo simple
			Simple = !Simple;
			write_eeprom(EE_SIMPLE, Simple);
			printf("\n\rModo Simple: ");
			if(Simple == TRUE) printf("ACTIVADO"); else printf("DESACTIVADO");
			printf("\n\r\n\r");
			break;
			
		case '?':
			printf("\n\r --- AYUDA ---\n\r\n\r");
			
			printf("Simbolos:\n\r");
			putc(SYM_HI);
			printf(" : Simbolo HIGH (11)\n\r");
			putc(SYM_HL);
			printf(" : Simbolo FLOAT (10)\n\r");
			putc(SYM_LH);
			printf(" : Simbolo LOW (00)\n\r");
			putc(SYM_LO);
			printf(" : Simbolo HIGH\n\r");
			putc(SYM_PU);
			printf(" : Simbolo PULSADO\n\r");
			putc(SYM_NP);
			printf(" : Simbolo NO PULSADO\n\r\n\r");
			
			printf("Configuraciones:\n\r");
			printf("4 / 6 / 8 : Configura cantidad de canales\n\r");
			printf("s : alterna visor de datos simples/avanzados\n\r");
			printf("? : Muestra ayuda\n\r\n\r");
			
			printf("Orden datos:\n\r");
			printf("4ch: D0 D1 D2 D3 A11 A10 A9 A8 A7 A6 A5 A4 A3 A2 A1 A0\n\r");
			printf("6ch: D0 D1 D2 D3 D4 D5 A9 A8 A7 A6 A5 A4 A3 A2 A1 A0\n\r");
			printf("8ch: D0 D1 D2 D3 D4 D5 D6 D7 A7 A6 A5 A4 A3 A2 A1 A0\n\r");
			
			printf(" -------------\n\r\n\r");
			
			
			break;
		
		default:
			printf("Comando desconocido. Usa ? para obtener ayuda.\n\r");
	}
	
	if((RcvChar == '4') || (RcvChar == '6') || (RcvChar == '8')){
		printf("\n\rDecodificador configurado para mando de %cCH\n\r\n\r", Modo);
	}
}

void LeerMem(void){
	Modo = read_eeprom(EE_MODO);
	Simple = read_eeprom(EE_SIMPLE);
}
/*void SimularPulsacion(void){
	Mantenido = TRUE;
	LED = ENCENDIDO;
	ActivarPulsadorTimeOut();
}

void ActivarPulsadorTimeOut(void){
	set_timer2(0);
	ContadorPulsadorTimeOut = 0;
}
*/
void ShowBIN(int32 val, int8 bits, int8 space, short head){
int x;

	if(head == TRUE){
		putc('0');
		putc('b');
	}
	
	for(x = bits; x > 0; x--){
		if(x%space == 0) putc(' ');	//espacio cada 8 bits
		
		if(bit_test(val, x-1) == 0)
			putc('0');
		else
			putc('1');
	}
}

void ShowTRIN(int32 val, int8 bits, int8 space, short head){
int x;

	if(head == TRUE){
		putc('0');
		putc('t');
	}
	
	for(x = bits; x > 1; x=x-2){
		if(x%space == 0) putc(' ');	//espacio cada 8 bits
		
		if(bit_test(val, x-1) == 0){		//empieza por 0
			if(bit_test(val, x-2) == 0){	//sigue por 0
				putc(SYM_LO);	// 00 -> LOW
			}
			else{							//sigue por 1
				putc(SYM_LH);	// 01 -> esto solo puede ser de un mando learning code
			}
		}
		else{								//empieza por 1
			if(bit_test(val, x-2) == 0){	//sigue por 0
				putc(SYM_HL);	// 10 -> flotando
			}
			else{							//sigue por 1
				putc(SYM_HI);	// 11 -> HIGH
			}
		}
	}
}