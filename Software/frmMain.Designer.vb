﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Me.btnAbrir = New System.Windows.Forms.Button()
		Me.cmbPuertos = New System.Windows.Forms.ComboBox()
		Me.Com = New System.IO.Ports.SerialPort(Me.components)
		Me.txtComm = New System.Windows.Forms.TextBox()
		Me.btnCerrar = New System.Windows.Forms.Button()
		Me.btnLimpiar = New System.Windows.Forms.Button()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.btnD0 = New System.Windows.Forms.Button()
		Me.btnD1 = New System.Windows.Forms.Button()
		Me.btnD2 = New System.Windows.Forms.Button()
		Me.btnD3 = New System.Windows.Forms.Button()
		Me.lbAddr5 = New System.Windows.Forms.Label()
		Me.lbAddr6 = New System.Windows.Forms.Label()
		Me.lbAddr7 = New System.Windows.Forms.Label()
		Me.lbAddr8 = New System.Windows.Forms.Label()
		Me.lbAddr3 = New System.Windows.Forms.Label()
		Me.lbAddr4 = New System.Windows.Forms.Label()
		Me.lbAddr1 = New System.Windows.Forms.Label()
		Me.lbAddr2 = New System.Windows.Forms.Label()
		Me.grpButtons = New System.Windows.Forms.GroupBox()
		Me.btnD7 = New System.Windows.Forms.Button()
		Me.btnD4 = New System.Windows.Forms.Button()
		Me.btnD5 = New System.Windows.Forms.Button()
		Me.btnD6 = New System.Windows.Forms.Button()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.grpCommunications = New System.Windows.Forms.GroupBox()
		Me.grpData = New System.Windows.Forms.GroupBox()
		Me.lbCode2 = New System.Windows.Forms.Label()
		Me.lbOrder = New System.Windows.Forms.Label()
		Me.lbPattern = New System.Windows.Forms.Label()
		Me.Label26 = New System.Windows.Forms.Label()
		Me.lbCode = New System.Windows.Forms.Label()
		Me.Label23 = New System.Windows.Forms.Label()
		Me.lbRcvDat = New System.Windows.Forms.Label()
		Me.Label24 = New System.Windows.Forms.Label()
		Me.lbBtn = New System.Windows.Forms.Label()
		Me.lbAddr = New System.Windows.Forms.Label()
		Me.grpAddress = New System.Windows.Forms.GroupBox()
		Me.lbAddr10 = New System.Windows.Forms.Label()
		Me.lbAddr9 = New System.Windows.Forms.Label()
		Me.lbAddrH = New System.Windows.Forms.Label()
		Me.lbAddrL = New System.Windows.Forms.Label()
		Me.chkInvPins = New System.Windows.Forms.CheckBox()
		Me.chkInvHL = New System.Windows.Forms.CheckBox()
		Me.lbTotTime = New System.Windows.Forms.Label()
		Me.grpTiming = New System.Windows.Forms.GroupBox()
		Me.lbTBS = New System.Windows.Forms.Label()
		Me.Label17 = New System.Windows.Forms.Label()
		Me.lbOscClk = New System.Windows.Forms.Label()
		Me.Label27 = New System.Windows.Forms.Label()
		Me.lbSyncTime = New System.Windows.Forms.Label()
		Me.Label25 = New System.Windows.Forms.Label()
		Me.lbBitTime = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.grpChannels = New System.Windows.Forms.GroupBox()
		Me.chk8CHinv = New System.Windows.Forms.CheckBox()
		Me.rb8CH = New System.Windows.Forms.RadioButton()
		Me.rb6CH = New System.Windows.Forms.RadioButton()
		Me.rb4CH = New System.Windows.Forms.RadioButton()
		Me.grpVisualSend = New System.Windows.Forms.GroupBox()
		Me.pnl10 = New System.Windows.Forms.Panel()
		Me.rbA10Fl = New System.Windows.Forms.RadioButton()
		Me.Label28 = New System.Windows.Forms.Label()
		Me.rbA10L = New System.Windows.Forms.RadioButton()
		Me.rbA10F = New System.Windows.Forms.RadioButton()
		Me.rbA10H = New System.Windows.Forms.RadioButton()
		Me.pnl9 = New System.Windows.Forms.Panel()
		Me.rbA9Fl = New System.Windows.Forms.RadioButton()
		Me.Label22 = New System.Windows.Forms.Label()
		Me.rbA9L = New System.Windows.Forms.RadioButton()
		Me.rbA9F = New System.Windows.Forms.RadioButton()
		Me.rbA9H = New System.Windows.Forms.RadioButton()
		Me.lbHexVisualSend = New System.Windows.Forms.Label()
		Me.Label21 = New System.Windows.Forms.Label()
		Me.chkLearning = New System.Windows.Forms.CheckBox()
		Me.Label20 = New System.Windows.Forms.Label()
		Me.btnVisualSend = New System.Windows.Forms.Button()
		Me.chkD7 = New System.Windows.Forms.CheckBox()
		Me.pnl8 = New System.Windows.Forms.Panel()
		Me.rbA8Fl = New System.Windows.Forms.RadioButton()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.rbA8L = New System.Windows.Forms.RadioButton()
		Me.rbA8F = New System.Windows.Forms.RadioButton()
		Me.rbA8H = New System.Windows.Forms.RadioButton()
		Me.chkD6 = New System.Windows.Forms.CheckBox()
		Me.pnl7 = New System.Windows.Forms.Panel()
		Me.rbA7Fl = New System.Windows.Forms.RadioButton()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.rbA7L = New System.Windows.Forms.RadioButton()
		Me.rbA7F = New System.Windows.Forms.RadioButton()
		Me.rbA7H = New System.Windows.Forms.RadioButton()
		Me.chkD5 = New System.Windows.Forms.CheckBox()
		Me.pnl6 = New System.Windows.Forms.Panel()
		Me.rbA6Fl = New System.Windows.Forms.RadioButton()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.rbA6L = New System.Windows.Forms.RadioButton()
		Me.rbA6F = New System.Windows.Forms.RadioButton()
		Me.rbA6H = New System.Windows.Forms.RadioButton()
		Me.chkD4 = New System.Windows.Forms.CheckBox()
		Me.pnl5 = New System.Windows.Forms.Panel()
		Me.rbA5Fl = New System.Windows.Forms.RadioButton()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.rbA5L = New System.Windows.Forms.RadioButton()
		Me.rbA5F = New System.Windows.Forms.RadioButton()
		Me.rbA5H = New System.Windows.Forms.RadioButton()
		Me.chkD3 = New System.Windows.Forms.CheckBox()
		Me.pnl4 = New System.Windows.Forms.Panel()
		Me.rbA4Fl = New System.Windows.Forms.RadioButton()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.rbA4L = New System.Windows.Forms.RadioButton()
		Me.rbA4F = New System.Windows.Forms.RadioButton()
		Me.rbA4H = New System.Windows.Forms.RadioButton()
		Me.chkD2 = New System.Windows.Forms.CheckBox()
		Me.pnl3 = New System.Windows.Forms.Panel()
		Me.rbA3Fl = New System.Windows.Forms.RadioButton()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.rbA3L = New System.Windows.Forms.RadioButton()
		Me.rbA3F = New System.Windows.Forms.RadioButton()
		Me.rbA3H = New System.Windows.Forms.RadioButton()
		Me.chkD1 = New System.Windows.Forms.CheckBox()
		Me.pnl2 = New System.Windows.Forms.Panel()
		Me.rbA2Fl = New System.Windows.Forms.RadioButton()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.rbA2L = New System.Windows.Forms.RadioButton()
		Me.rbA2F = New System.Windows.Forms.RadioButton()
		Me.rbA2H = New System.Windows.Forms.RadioButton()
		Me.chkD0 = New System.Windows.Forms.CheckBox()
		Me.pnl1 = New System.Windows.Forms.Panel()
		Me.rbA1Fl = New System.Windows.Forms.RadioButton()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.rbA1L = New System.Windows.Forms.RadioButton()
		Me.rbA1F = New System.Windows.Forms.RadioButton()
		Me.rbA1H = New System.Windows.Forms.RadioButton()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.txtHexSend = New System.Windows.Forms.TextBox()
		Me.Label16 = New System.Windows.Forms.Label()
		Me.grpHexSend = New System.Windows.Forms.GroupBox()
		Me.btnHexClear = New System.Windows.Forms.Button()
		Me.btnHexSend = New System.Windows.Forms.Button()
		Me.grpCfg = New System.Windows.Forms.GroupBox()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.txtNumPulses = New System.Windows.Forms.NumericUpDown()
		Me.Label18 = New System.Windows.Forms.Label()
		Me.rd32mS = New System.Windows.Forms.RadioButton()
		Me.rd16mS = New System.Windows.Forms.RadioButton()
		Me.rd48mS = New System.Windows.Forms.RadioButton()
		Me.rd64mS = New System.Windows.Forms.RadioButton()
		Me.grpButtons.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.grpCommunications.SuspendLayout()
		Me.grpData.SuspendLayout()
		Me.grpAddress.SuspendLayout()
		Me.grpTiming.SuspendLayout()
		Me.grpChannels.SuspendLayout()
		Me.grpVisualSend.SuspendLayout()
		Me.pnl10.SuspendLayout()
		Me.pnl9.SuspendLayout()
		Me.pnl8.SuspendLayout()
		Me.pnl7.SuspendLayout()
		Me.pnl6.SuspendLayout()
		Me.pnl5.SuspendLayout()
		Me.pnl4.SuspendLayout()
		Me.pnl3.SuspendLayout()
		Me.pnl2.SuspendLayout()
		Me.pnl1.SuspendLayout()
		Me.grpHexSend.SuspendLayout()
		Me.grpCfg.SuspendLayout()
		CType(Me.txtNumPulses, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'btnAbrir
		'
		Me.btnAbrir.Location = New System.Drawing.Point(112, 21)
		Me.btnAbrir.Name = "btnAbrir"
		Me.btnAbrir.Size = New System.Drawing.Size(44, 22)
		Me.btnAbrir.TabIndex = 0
		Me.btnAbrir.Text = "Open"
		Me.btnAbrir.UseVisualStyleBackColor = True
		'
		'cmbPuertos
		'
		Me.cmbPuertos.FormattingEnabled = True
		Me.cmbPuertos.Location = New System.Drawing.Point(10, 22)
		Me.cmbPuertos.Name = "cmbPuertos"
		Me.cmbPuertos.Size = New System.Drawing.Size(96, 21)
		Me.cmbPuertos.TabIndex = 1
		'
		'Com
		'
		Me.Com.BaudRate = 38400
		Me.Com.ReadBufferSize = 130
		Me.Com.WriteBufferSize = 130
		'
		'txtComm
		'
		Me.txtComm.Location = New System.Drawing.Point(6, 19)
		Me.txtComm.Multiline = True
		Me.txtComm.Name = "txtComm"
		Me.txtComm.ScrollBars = System.Windows.Forms.ScrollBars.Both
		Me.txtComm.Size = New System.Drawing.Size(588, 81)
		Me.txtComm.TabIndex = 27
		'
		'btnCerrar
		'
		Me.btnCerrar.Location = New System.Drawing.Point(162, 21)
		Me.btnCerrar.Name = "btnCerrar"
		Me.btnCerrar.Size = New System.Drawing.Size(44, 22)
		Me.btnCerrar.TabIndex = 2
		Me.btnCerrar.Text = "Close"
		Me.btnCerrar.UseVisualStyleBackColor = True
		'
		'btnLimpiar
		'
		Me.btnLimpiar.Location = New System.Drawing.Point(601, 19)
		Me.btnLimpiar.Name = "btnLimpiar"
		Me.btnLimpiar.Size = New System.Drawing.Size(62, 81)
		Me.btnLimpiar.TabIndex = 28
		Me.btnLimpiar.Text = "Clean"
		Me.btnLimpiar.UseVisualStyleBackColor = True
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(17, 137)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(38, 13)
		Me.Label1.TabIndex = 31
		Me.Label1.Text = "Button"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(17, 24)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(57, 13)
		Me.Label2.TabIndex = 32
		Me.Label2.Text = "Total Time"
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(17, 167)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(45, 13)
		Me.Label3.TabIndex = 34
		Me.Label3.Text = "Address"
		'
		'btnD0
		'
		Me.btnD0.BackColor = System.Drawing.Color.White
		Me.btnD0.Enabled = False
		Me.btnD0.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.btnD0.Location = New System.Drawing.Point(25, 23)
		Me.btnD0.Name = "btnD0"
		Me.btnD0.Size = New System.Drawing.Size(29, 23)
		Me.btnD0.TabIndex = 35
		Me.btnD0.TabStop = False
		Me.btnD0.Text = "D0"
		Me.btnD0.UseVisualStyleBackColor = False
		'
		'btnD1
		'
		Me.btnD1.BackColor = System.Drawing.Color.White
		Me.btnD1.Enabled = False
		Me.btnD1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.btnD1.Location = New System.Drawing.Point(62, 23)
		Me.btnD1.Name = "btnD1"
		Me.btnD1.Size = New System.Drawing.Size(29, 23)
		Me.btnD1.TabIndex = 36
		Me.btnD1.TabStop = False
		Me.btnD1.Text = "D1"
		Me.btnD1.UseVisualStyleBackColor = False
		'
		'btnD2
		'
		Me.btnD2.BackColor = System.Drawing.Color.White
		Me.btnD2.Enabled = False
		Me.btnD2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.btnD2.Location = New System.Drawing.Point(99, 23)
		Me.btnD2.Name = "btnD2"
		Me.btnD2.Size = New System.Drawing.Size(29, 23)
		Me.btnD2.TabIndex = 37
		Me.btnD2.TabStop = False
		Me.btnD2.Text = "D2"
		Me.btnD2.UseVisualStyleBackColor = False
		'
		'btnD3
		'
		Me.btnD3.BackColor = System.Drawing.Color.White
		Me.btnD3.Enabled = False
		Me.btnD3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.btnD3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.btnD3.Location = New System.Drawing.Point(136, 23)
		Me.btnD3.Name = "btnD3"
		Me.btnD3.Size = New System.Drawing.Size(29, 23)
		Me.btnD3.TabIndex = 38
		Me.btnD3.TabStop = False
		Me.btnD3.Text = "D3"
		Me.btnD3.UseVisualStyleBackColor = False
		'
		'lbAddr5
		'
		Me.lbAddr5.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr5.Location = New System.Drawing.Point(63, 20)
		Me.lbAddr5.Name = "lbAddr5"
		Me.lbAddr5.Size = New System.Drawing.Size(10, 16)
		Me.lbAddr5.TabIndex = 6
		Me.lbAddr5.Text = "5"
		Me.lbAddr5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddr6
		'
		Me.lbAddr6.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr6.Location = New System.Drawing.Point(75, 20)
		Me.lbAddr6.Name = "lbAddr6"
		Me.lbAddr6.Size = New System.Drawing.Size(10, 16)
		Me.lbAddr6.TabIndex = 7
		Me.lbAddr6.Text = "6"
		Me.lbAddr6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddr7
		'
		Me.lbAddr7.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr7.Location = New System.Drawing.Point(87, 20)
		Me.lbAddr7.Name = "lbAddr7"
		Me.lbAddr7.Size = New System.Drawing.Size(10, 16)
		Me.lbAddr7.TabIndex = 4
		Me.lbAddr7.Text = "7"
		Me.lbAddr7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddr8
		'
		Me.lbAddr8.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr8.Location = New System.Drawing.Point(99, 20)
		Me.lbAddr8.Name = "lbAddr8"
		Me.lbAddr8.Size = New System.Drawing.Size(10, 16)
		Me.lbAddr8.TabIndex = 5
		Me.lbAddr8.Text = "8"
		Me.lbAddr8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddr3
		'
		Me.lbAddr3.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr3.Location = New System.Drawing.Point(39, 20)
		Me.lbAddr3.Name = "lbAddr3"
		Me.lbAddr3.Size = New System.Drawing.Size(10, 16)
		Me.lbAddr3.TabIndex = 2
		Me.lbAddr3.Text = "3"
		Me.lbAddr3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddr4
		'
		Me.lbAddr4.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr4.Location = New System.Drawing.Point(51, 20)
		Me.lbAddr4.Name = "lbAddr4"
		Me.lbAddr4.Size = New System.Drawing.Size(10, 16)
		Me.lbAddr4.TabIndex = 6
		Me.lbAddr4.Text = "4"
		Me.lbAddr4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddr1
		'
		Me.lbAddr1.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr1.Location = New System.Drawing.Point(15, 20)
		Me.lbAddr1.Name = "lbAddr1"
		Me.lbAddr1.Size = New System.Drawing.Size(10, 16)
		Me.lbAddr1.TabIndex = 0
		Me.lbAddr1.Text = "1"
		Me.lbAddr1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddr2
		'
		Me.lbAddr2.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr2.Location = New System.Drawing.Point(27, 20)
		Me.lbAddr2.Name = "lbAddr2"
		Me.lbAddr2.Size = New System.Drawing.Size(10, 16)
		Me.lbAddr2.TabIndex = 1
		Me.lbAddr2.Text = "2"
		Me.lbAddr2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'grpButtons
		'
		Me.grpButtons.Controls.Add(Me.btnD7)
		Me.grpButtons.Controls.Add(Me.btnD4)
		Me.grpButtons.Controls.Add(Me.btnD5)
		Me.grpButtons.Controls.Add(Me.btnD6)
		Me.grpButtons.Controls.Add(Me.btnD3)
		Me.grpButtons.Controls.Add(Me.btnD0)
		Me.grpButtons.Controls.Add(Me.btnD1)
		Me.grpButtons.Controls.Add(Me.btnD2)
		Me.grpButtons.Location = New System.Drawing.Point(12, 364)
		Me.grpButtons.Name = "grpButtons"
		Me.grpButtons.Size = New System.Drawing.Size(186, 87)
		Me.grpButtons.TabIndex = 45
		Me.grpButtons.TabStop = False
		Me.grpButtons.Text = "Buttons"
		'
		'btnD7
		'
		Me.btnD7.BackColor = System.Drawing.Color.White
		Me.btnD7.Enabled = False
		Me.btnD7.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.btnD7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.btnD7.Location = New System.Drawing.Point(136, 52)
		Me.btnD7.Name = "btnD7"
		Me.btnD7.Size = New System.Drawing.Size(29, 23)
		Me.btnD7.TabIndex = 42
		Me.btnD7.TabStop = False
		Me.btnD7.Text = "D7"
		Me.btnD7.UseVisualStyleBackColor = False
		'
		'btnD4
		'
		Me.btnD4.BackColor = System.Drawing.Color.White
		Me.btnD4.Enabled = False
		Me.btnD4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.btnD4.Location = New System.Drawing.Point(25, 52)
		Me.btnD4.Name = "btnD4"
		Me.btnD4.Size = New System.Drawing.Size(29, 23)
		Me.btnD4.TabIndex = 39
		Me.btnD4.TabStop = False
		Me.btnD4.Text = "D4"
		Me.btnD4.UseVisualStyleBackColor = False
		'
		'btnD5
		'
		Me.btnD5.BackColor = System.Drawing.Color.White
		Me.btnD5.Enabled = False
		Me.btnD5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.btnD5.Location = New System.Drawing.Point(62, 52)
		Me.btnD5.Name = "btnD5"
		Me.btnD5.Size = New System.Drawing.Size(29, 23)
		Me.btnD5.TabIndex = 40
		Me.btnD5.TabStop = False
		Me.btnD5.Text = "D5"
		Me.btnD5.UseVisualStyleBackColor = False
		'
		'btnD6
		'
		Me.btnD6.BackColor = System.Drawing.Color.White
		Me.btnD6.Enabled = False
		Me.btnD6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
		Me.btnD6.Location = New System.Drawing.Point(99, 52)
		Me.btnD6.Name = "btnD6"
		Me.btnD6.Size = New System.Drawing.Size(29, 23)
		Me.btnD6.TabIndex = 41
		Me.btnD6.TabStop = False
		Me.btnD6.Text = "D6"
		Me.btnD6.UseVisualStyleBackColor = False
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.btnAbrir)
		Me.GroupBox2.Controls.Add(Me.cmbPuertos)
		Me.GroupBox2.Controls.Add(Me.btnCerrar)
		Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(215, 56)
		Me.GroupBox2.TabIndex = 46
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "Serial Port"
		'
		'grpCommunications
		'
		Me.grpCommunications.Controls.Add(Me.txtComm)
		Me.grpCommunications.Controls.Add(Me.btnLimpiar)
		Me.grpCommunications.Location = New System.Drawing.Point(12, 457)
		Me.grpCommunications.Name = "grpCommunications"
		Me.grpCommunications.Size = New System.Drawing.Size(674, 107)
		Me.grpCommunications.TabIndex = 48
		Me.grpCommunications.TabStop = False
		Me.grpCommunications.Text = "Communications"
		'
		'grpData
		'
		Me.grpData.Controls.Add(Me.lbCode2)
		Me.grpData.Controls.Add(Me.lbOrder)
		Me.grpData.Controls.Add(Me.lbPattern)
		Me.grpData.Controls.Add(Me.Label26)
		Me.grpData.Controls.Add(Me.lbCode)
		Me.grpData.Controls.Add(Me.Label23)
		Me.grpData.Controls.Add(Me.lbRcvDat)
		Me.grpData.Controls.Add(Me.Label3)
		Me.grpData.Controls.Add(Me.Label24)
		Me.grpData.Controls.Add(Me.lbBtn)
		Me.grpData.Controls.Add(Me.lbAddr)
		Me.grpData.Controls.Add(Me.Label1)
		Me.grpData.Location = New System.Drawing.Point(12, 74)
		Me.grpData.Name = "grpData"
		Me.grpData.Size = New System.Drawing.Size(429, 197)
		Me.grpData.TabIndex = 50
		Me.grpData.TabStop = False
		Me.grpData.Text = "Data"
		'
		'lbCode2
		'
		Me.lbCode2.AutoSize = True
		Me.lbCode2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbCode2.Location = New System.Drawing.Point(122, 105)
		Me.lbCode2.Name = "lbCode2"
		Me.lbCode2.Size = New System.Drawing.Size(212, 17)
		Me.lbCode2.TabIndex = 52
		Me.lbCode2.Text = "(8 bits buttons / 16 bits address)"
		'
		'lbOrder
		'
		Me.lbOrder.AutoSize = True
		Me.lbOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbOrder.Location = New System.Drawing.Point(76, 39)
		Me.lbOrder.Name = "lbOrder"
		Me.lbOrder.Size = New System.Drawing.Size(230, 17)
		Me.lbOrder.TabIndex = 51
		Me.lbOrder.Text = "     |     Btn     |          Address          |"
		'
		'lbPattern
		'
		Me.lbPattern.AutoSize = True
		Me.lbPattern.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbPattern.Location = New System.Drawing.Point(76, 75)
		Me.lbPattern.Name = "lbPattern"
		Me.lbPattern.Size = New System.Drawing.Size(227, 17)
		Me.lbPattern.TabIndex = 50
		Me.lbPattern.Text = "D0 ~ D3 / A7 ~ A0 <- LSB sent first"
		'
		'Label26
		'
		Me.Label26.AutoSize = True
		Me.Label26.Location = New System.Drawing.Point(17, 77)
		Me.Label26.Name = "Label26"
		Me.Label26.Size = New System.Drawing.Size(41, 13)
		Me.Label26.TabIndex = 49
		Me.Label26.Text = "Pattern"
		'
		'lbCode
		'
		Me.lbCode.AutoSize = True
		Me.lbCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbCode.Location = New System.Drawing.Point(76, 105)
		Me.lbCode.Name = "lbCode"
		Me.lbCode.Size = New System.Drawing.Size(46, 17)
		Me.lbCode.TabIndex = 48
		Me.lbCode.Text = "Fixed"
		'
		'Label23
		'
		Me.Label23.AutoSize = True
		Me.Label23.Location = New System.Drawing.Point(17, 107)
		Me.Label23.Name = "Label23"
		Me.Label23.Size = New System.Drawing.Size(32, 13)
		Me.Label23.TabIndex = 47
		Me.Label23.Text = "Code"
		'
		'lbRcvDat
		'
		Me.lbRcvDat.AutoSize = True
		Me.lbRcvDat.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbRcvDat.Location = New System.Drawing.Point(76, 22)
		Me.lbRcvDat.Name = "lbRcvDat"
		Me.lbRcvDat.Size = New System.Drawing.Size(316, 17)
		Me.lbRcvDat.TabIndex = 46
		Me.lbRcvDat.Text = "0b 00000000 00000000 00000000 (0x 00 00 00)"
		'
		'Label24
		'
		Me.Label24.AutoSize = True
		Me.Label24.Location = New System.Drawing.Point(17, 24)
		Me.Label24.Name = "Label24"
		Me.Label24.Size = New System.Drawing.Size(53, 13)
		Me.Label24.TabIndex = 45
		Me.Label24.Text = "Received"
		'
		'lbBtn
		'
		Me.lbBtn.AutoSize = True
		Me.lbBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbBtn.Location = New System.Drawing.Point(76, 135)
		Me.lbBtn.Name = "lbBtn"
		Me.lbBtn.Size = New System.Drawing.Size(140, 17)
		Me.lbBtn.TabIndex = 41
		Me.lbBtn.Text = "0b 00000000 (0x 00)"
		Me.lbBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddr
		'
		Me.lbAddr.AutoSize = True
		Me.lbAddr.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbAddr.Location = New System.Drawing.Point(76, 165)
		Me.lbAddr.Name = "lbAddr"
		Me.lbAddr.Size = New System.Drawing.Size(228, 17)
		Me.lbAddr.TabIndex = 38
		Me.lbAddr.Text = "0b 00000000 00000000 (0x 00 00)"
		Me.lbAddr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'grpAddress
		'
		Me.grpAddress.Controls.Add(Me.lbAddr10)
		Me.grpAddress.Controls.Add(Me.lbAddr9)
		Me.grpAddress.Controls.Add(Me.lbAddrH)
		Me.grpAddress.Controls.Add(Me.lbAddrL)
		Me.grpAddress.Controls.Add(Me.chkInvPins)
		Me.grpAddress.Controls.Add(Me.chkInvHL)
		Me.grpAddress.Controls.Add(Me.lbAddr1)
		Me.grpAddress.Controls.Add(Me.lbAddr2)
		Me.grpAddress.Controls.Add(Me.lbAddr3)
		Me.grpAddress.Controls.Add(Me.lbAddr4)
		Me.grpAddress.Controls.Add(Me.lbAddr5)
		Me.grpAddress.Controls.Add(Me.lbAddr8)
		Me.grpAddress.Controls.Add(Me.lbAddr7)
		Me.grpAddress.Controls.Add(Me.lbAddr6)
		Me.grpAddress.Location = New System.Drawing.Point(204, 364)
		Me.grpAddress.Name = "grpAddress"
		Me.grpAddress.Size = New System.Drawing.Size(237, 87)
		Me.grpAddress.TabIndex = 53
		Me.grpAddress.TabStop = False
		Me.grpAddress.Text = "Address"
		'
		'lbAddr10
		'
		Me.lbAddr10.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr10.Location = New System.Drawing.Point(123, 20)
		Me.lbAddr10.Name = "lbAddr10"
		Me.lbAddr10.Size = New System.Drawing.Size(20, 16)
		Me.lbAddr10.TabIndex = 11
		Me.lbAddr10.Text = "10"
		Me.lbAddr10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddr9
		'
		Me.lbAddr9.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.lbAddr9.Location = New System.Drawing.Point(111, 20)
		Me.lbAddr9.Name = "lbAddr9"
		Me.lbAddr9.Size = New System.Drawing.Size(10, 16)
		Me.lbAddr9.TabIndex = 10
		Me.lbAddr9.Text = "9"
		Me.lbAddr9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddrH
		'
		Me.lbAddrH.Location = New System.Drawing.Point(15, 34)
		Me.lbAddrH.Name = "lbAddrH"
		Me.lbAddrH.Size = New System.Drawing.Size(10, 15)
		Me.lbAddrH.TabIndex = 9
		Me.lbAddrH.Text = "H"
		Me.lbAddrH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'lbAddrL
		'
		Me.lbAddrL.Location = New System.Drawing.Point(13, 49)
		Me.lbAddrL.Name = "lbAddrL"
		Me.lbAddrL.Size = New System.Drawing.Size(10, 15)
		Me.lbAddrL.TabIndex = 8
		Me.lbAddrL.Text = "L"
		Me.lbAddrL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'chkInvPins
		'
		Me.chkInvPins.AutoSize = True
		Me.chkInvPins.Location = New System.Drawing.Point(156, 61)
		Me.chkInvPins.Name = "chkInvPins"
		Me.chkInvPins.Size = New System.Drawing.Size(76, 17)
		Me.chkInvPins.TabIndex = 1
		Me.chkInvPins.Text = "Invert Pins"
		Me.chkInvPins.UseVisualStyleBackColor = True
		'
		'chkInvHL
		'
		Me.chkInvHL.AutoSize = True
		Me.chkInvHL.Location = New System.Drawing.Point(156, 34)
		Me.chkInvHL.Name = "chkInvHL"
		Me.chkInvHL.Size = New System.Drawing.Size(75, 17)
		Me.chkInvHL.TabIndex = 0
		Me.chkInvHL.Text = "Invert H/L"
		Me.chkInvHL.UseVisualStyleBackColor = True
		'
		'lbTotTime
		'
		Me.lbTotTime.AutoSize = True
		Me.lbTotTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbTotTime.Location = New System.Drawing.Point(76, 22)
		Me.lbTotTime.Name = "lbTotTime"
		Me.lbTotTime.Size = New System.Drawing.Size(40, 17)
		Me.lbTotTime.TabIndex = 44
		Me.lbTotTime.Text = "0 mS"
		'
		'grpTiming
		'
		Me.grpTiming.Controls.Add(Me.lbTBS)
		Me.grpTiming.Controls.Add(Me.Label17)
		Me.grpTiming.Controls.Add(Me.lbOscClk)
		Me.grpTiming.Controls.Add(Me.Label27)
		Me.grpTiming.Controls.Add(Me.lbSyncTime)
		Me.grpTiming.Controls.Add(Me.Label25)
		Me.grpTiming.Controls.Add(Me.lbBitTime)
		Me.grpTiming.Controls.Add(Me.Label4)
		Me.grpTiming.Controls.Add(Me.Label2)
		Me.grpTiming.Controls.Add(Me.lbTotTime)
		Me.grpTiming.Location = New System.Drawing.Point(12, 277)
		Me.grpTiming.Name = "grpTiming"
		Me.grpTiming.Size = New System.Drawing.Size(429, 81)
		Me.grpTiming.TabIndex = 51
		Me.grpTiming.TabStop = False
		Me.grpTiming.Text = "Timing"
		'
		'lbTBS
		'
		Me.lbTBS.AutoSize = True
		Me.lbTBS.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbTBS.Location = New System.Drawing.Point(326, 52)
		Me.lbTBS.Name = "lbTBS"
		Me.lbTBS.Size = New System.Drawing.Size(40, 17)
		Me.lbTBS.TabIndex = 52
		Me.lbTBS.Text = "0 mS"
		'
		'Label17
		'
		Me.Label17.AutoSize = True
		Me.Label17.Location = New System.Drawing.Point(301, 24)
		Me.Label17.Name = "Label17"
		Me.Label17.Size = New System.Drawing.Size(112, 13)
		Me.Label17.TabIndex = 51
		Me.Label17.Text = "Time Between Signals"
		'
		'lbOscClk
		'
		Me.lbOscClk.AutoSize = True
		Me.lbOscClk.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbOscClk.Location = New System.Drawing.Point(221, 22)
		Me.lbOscClk.Name = "lbOscClk"
		Me.lbOscClk.Size = New System.Drawing.Size(40, 17)
		Me.lbOscClk.TabIndex = 50
		Me.lbOscClk.Text = "0 mS"
		'
		'Label27
		'
		Me.Label27.AutoSize = True
		Me.Label27.Location = New System.Drawing.Point(173, 24)
		Me.Label27.Name = "Label27"
		Me.Label27.Size = New System.Drawing.Size(44, 13)
		Me.Label27.TabIndex = 49
		Me.Label27.Text = "Osc Clk"
		'
		'lbSyncTime
		'
		Me.lbSyncTime.AutoSize = True
		Me.lbSyncTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbSyncTime.Location = New System.Drawing.Point(76, 52)
		Me.lbSyncTime.Name = "lbSyncTime"
		Me.lbSyncTime.Size = New System.Drawing.Size(40, 17)
		Me.lbSyncTime.TabIndex = 48
		Me.lbSyncTime.Text = "0 mS"
		'
		'Label25
		'
		Me.Label25.AutoSize = True
		Me.Label25.Location = New System.Drawing.Point(17, 54)
		Me.Label25.Name = "Label25"
		Me.Label25.Size = New System.Drawing.Size(57, 13)
		Me.Label25.TabIndex = 47
		Me.Label25.Text = "Sync Time"
		'
		'lbBitTime
		'
		Me.lbBitTime.AutoSize = True
		Me.lbBitTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lbBitTime.Location = New System.Drawing.Point(221, 52)
		Me.lbBitTime.Name = "lbBitTime"
		Me.lbBitTime.Size = New System.Drawing.Size(40, 17)
		Me.lbBitTime.TabIndex = 46
		Me.lbBitTime.Text = "0 mS"
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(173, 54)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(45, 13)
		Me.Label4.TabIndex = 45
		Me.Label4.Text = "Bit Time"
		'
		'grpChannels
		'
		Me.grpChannels.Controls.Add(Me.chk8CHinv)
		Me.grpChannels.Controls.Add(Me.rb8CH)
		Me.grpChannels.Controls.Add(Me.rb6CH)
		Me.grpChannels.Controls.Add(Me.rb4CH)
		Me.grpChannels.Location = New System.Drawing.Point(234, 12)
		Me.grpChannels.Name = "grpChannels"
		Me.grpChannels.Size = New System.Drawing.Size(207, 56)
		Me.grpChannels.TabIndex = 52
		Me.grpChannels.TabStop = False
		Me.grpChannels.Text = "Remote Channels"
		'
		'chk8CHinv
		'
		Me.chk8CHinv.AutoSize = True
		Me.chk8CHinv.Enabled = False
		Me.chk8CHinv.Location = New System.Drawing.Point(112, 36)
		Me.chk8CHinv.Name = "chk8CHinv"
		Me.chk8CHinv.Size = New System.Drawing.Size(66, 17)
		Me.chk8CHinv.TabIndex = 3
		Me.chk8CHinv.Text = "8 Ch Inv"
		Me.chk8CHinv.UseVisualStyleBackColor = True
		'
		'rb8CH
		'
		Me.rb8CH.AutoSize = True
		Me.rb8CH.Location = New System.Drawing.Point(37, 35)
		Me.rb8CH.Name = "rb8CH"
		Me.rb8CH.Size = New System.Drawing.Size(47, 17)
		Me.rb8CH.TabIndex = 2
		Me.rb8CH.Text = "8 Ch"
		Me.rb8CH.UseVisualStyleBackColor = True
		'
		'rb6CH
		'
		Me.rb6CH.AutoSize = True
		Me.rb6CH.Location = New System.Drawing.Point(113, 15)
		Me.rb6CH.Name = "rb6CH"
		Me.rb6CH.Size = New System.Drawing.Size(47, 17)
		Me.rb6CH.TabIndex = 1
		Me.rb6CH.Text = "6 Ch"
		Me.rb6CH.UseVisualStyleBackColor = True
		'
		'rb4CH
		'
		Me.rb4CH.AutoSize = True
		Me.rb4CH.Checked = True
		Me.rb4CH.Location = New System.Drawing.Point(37, 15)
		Me.rb4CH.Name = "rb4CH"
		Me.rb4CH.Size = New System.Drawing.Size(47, 17)
		Me.rb4CH.TabIndex = 0
		Me.rb4CH.TabStop = True
		Me.rb4CH.Text = "4 Ch"
		Me.rb4CH.UseVisualStyleBackColor = True
		'
		'grpVisualSend
		'
		Me.grpVisualSend.Controls.Add(Me.pnl10)
		Me.grpVisualSend.Controls.Add(Me.pnl9)
		Me.grpVisualSend.Controls.Add(Me.lbHexVisualSend)
		Me.grpVisualSend.Controls.Add(Me.Label21)
		Me.grpVisualSend.Controls.Add(Me.chkLearning)
		Me.grpVisualSend.Controls.Add(Me.Label20)
		Me.grpVisualSend.Controls.Add(Me.btnVisualSend)
		Me.grpVisualSend.Controls.Add(Me.chkD7)
		Me.grpVisualSend.Controls.Add(Me.pnl8)
		Me.grpVisualSend.Controls.Add(Me.chkD6)
		Me.grpVisualSend.Controls.Add(Me.pnl7)
		Me.grpVisualSend.Controls.Add(Me.chkD5)
		Me.grpVisualSend.Controls.Add(Me.pnl6)
		Me.grpVisualSend.Controls.Add(Me.chkD4)
		Me.grpVisualSend.Controls.Add(Me.pnl5)
		Me.grpVisualSend.Controls.Add(Me.chkD3)
		Me.grpVisualSend.Controls.Add(Me.pnl4)
		Me.grpVisualSend.Controls.Add(Me.chkD2)
		Me.grpVisualSend.Controls.Add(Me.pnl3)
		Me.grpVisualSend.Controls.Add(Me.chkD1)
		Me.grpVisualSend.Controls.Add(Me.pnl2)
		Me.grpVisualSend.Controls.Add(Me.chkD0)
		Me.grpVisualSend.Controls.Add(Me.pnl1)
		Me.grpVisualSend.Controls.Add(Me.Label7)
		Me.grpVisualSend.Controls.Add(Me.Label6)
		Me.grpVisualSend.Controls.Add(Me.Label5)
		Me.grpVisualSend.Location = New System.Drawing.Point(449, 117)
		Me.grpVisualSend.Name = "grpVisualSend"
		Me.grpVisualSend.Size = New System.Drawing.Size(237, 263)
		Me.grpVisualSend.TabIndex = 54
		Me.grpVisualSend.TabStop = False
		Me.grpVisualSend.Text = "Visual Send"
		'
		'pnl10
		'
		Me.pnl10.Controls.Add(Me.rbA10Fl)
		Me.pnl10.Controls.Add(Me.Label28)
		Me.pnl10.Controls.Add(Me.rbA10L)
		Me.pnl10.Controls.Add(Me.rbA10F)
		Me.pnl10.Controls.Add(Me.rbA10H)
		Me.pnl10.Enabled = False
		Me.pnl10.Location = New System.Drawing.Point(212, 53)
		Me.pnl10.Name = "pnl10"
		Me.pnl10.Size = New System.Drawing.Size(18, 124)
		Me.pnl10.TabIndex = 6
		'
		'rbA10Fl
		'
		Me.rbA10Fl.AutoSize = True
		Me.rbA10Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA10Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA10Fl.Name = "rbA10Fl"
		Me.rbA10Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA10Fl.TabIndex = 5
		Me.rbA10Fl.UseVisualStyleBackColor = False
		'
		'Label28
		'
		Me.Label28.AutoSize = True
		Me.Label28.Location = New System.Drawing.Point(-1, 3)
		Me.Label28.Name = "Label28"
		Me.Label28.Size = New System.Drawing.Size(19, 13)
		Me.Label28.TabIndex = 3
		Me.Label28.Text = "10"
		'
		'rbA10L
		'
		Me.rbA10L.AutoSize = True
		Me.rbA10L.Location = New System.Drawing.Point(3, 106)
		Me.rbA10L.Name = "rbA10L"
		Me.rbA10L.Size = New System.Drawing.Size(14, 13)
		Me.rbA10L.TabIndex = 2
		Me.rbA10L.UseVisualStyleBackColor = True
		'
		'rbA10F
		'
		Me.rbA10F.AutoSize = True
		Me.rbA10F.Checked = True
		Me.rbA10F.Location = New System.Drawing.Point(3, 50)
		Me.rbA10F.Name = "rbA10F"
		Me.rbA10F.Size = New System.Drawing.Size(14, 13)
		Me.rbA10F.TabIndex = 1
		Me.rbA10F.TabStop = True
		Me.rbA10F.UseVisualStyleBackColor = True
		'
		'rbA10H
		'
		Me.rbA10H.AutoSize = True
		Me.rbA10H.Location = New System.Drawing.Point(3, 22)
		Me.rbA10H.Name = "rbA10H"
		Me.rbA10H.Size = New System.Drawing.Size(14, 13)
		Me.rbA10H.TabIndex = 0
		Me.rbA10H.UseVisualStyleBackColor = True
		'
		'pnl9
		'
		Me.pnl9.Controls.Add(Me.rbA9Fl)
		Me.pnl9.Controls.Add(Me.Label22)
		Me.pnl9.Controls.Add(Me.rbA9L)
		Me.pnl9.Controls.Add(Me.rbA9F)
		Me.pnl9.Controls.Add(Me.rbA9H)
		Me.pnl9.Enabled = False
		Me.pnl9.Location = New System.Drawing.Point(191, 53)
		Me.pnl9.Name = "pnl9"
		Me.pnl9.Size = New System.Drawing.Size(18, 124)
		Me.pnl9.TabIndex = 6
		'
		'rbA9Fl
		'
		Me.rbA9Fl.AutoSize = True
		Me.rbA9Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA9Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA9Fl.Name = "rbA9Fl"
		Me.rbA9Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA9Fl.TabIndex = 5
		Me.rbA9Fl.UseVisualStyleBackColor = False
		'
		'Label22
		'
		Me.Label22.AutoSize = True
		Me.Label22.Location = New System.Drawing.Point(3, 3)
		Me.Label22.Name = "Label22"
		Me.Label22.Size = New System.Drawing.Size(13, 13)
		Me.Label22.TabIndex = 3
		Me.Label22.Text = "9"
		'
		'rbA9L
		'
		Me.rbA9L.AutoSize = True
		Me.rbA9L.Location = New System.Drawing.Point(3, 106)
		Me.rbA9L.Name = "rbA9L"
		Me.rbA9L.Size = New System.Drawing.Size(14, 13)
		Me.rbA9L.TabIndex = 2
		Me.rbA9L.UseVisualStyleBackColor = True
		'
		'rbA9F
		'
		Me.rbA9F.AutoSize = True
		Me.rbA9F.Checked = True
		Me.rbA9F.Location = New System.Drawing.Point(3, 50)
		Me.rbA9F.Name = "rbA9F"
		Me.rbA9F.Size = New System.Drawing.Size(14, 13)
		Me.rbA9F.TabIndex = 1
		Me.rbA9F.TabStop = True
		Me.rbA9F.UseVisualStyleBackColor = True
		'
		'rbA9H
		'
		Me.rbA9H.AutoSize = True
		Me.rbA9H.Location = New System.Drawing.Point(3, 22)
		Me.rbA9H.Name = "rbA9H"
		Me.rbA9H.Size = New System.Drawing.Size(14, 13)
		Me.rbA9H.TabIndex = 0
		Me.rbA9H.UseVisualStyleBackColor = True
		'
		'lbHexVisualSend
		'
		Me.lbHexVisualSend.AutoSize = True
		Me.lbHexVisualSend.Location = New System.Drawing.Point(150, 33)
		Me.lbHexVisualSend.Name = "lbHexVisualSend"
		Me.lbHexVisualSend.Size = New System.Drawing.Size(63, 13)
		Me.lbHexVisualSend.TabIndex = 67
		Me.lbHexVisualSend.Text = "0x 00 00 00"
		'
		'Label21
		'
		Me.Label21.AutoSize = True
		Me.Label21.Location = New System.Drawing.Point(153, 16)
		Me.Label21.Name = "Label21"
		Me.Label21.Size = New System.Drawing.Size(59, 13)
		Me.Label21.TabIndex = 66
		Me.Label21.Text = "Hex Value:"
		'
		'chkLearning
		'
		Me.chkLearning.AutoSize = True
		Me.chkLearning.Location = New System.Drawing.Point(37, 25)
		Me.chkLearning.Name = "chkLearning"
		Me.chkLearning.Size = New System.Drawing.Size(95, 17)
		Me.chkLearning.TabIndex = 65
		Me.chkLearning.Text = "Learning Code"
		Me.chkLearning.UseVisualStyleBackColor = True
		'
		'Label20
		'
		Me.Label20.AutoSize = True
		Me.Label20.Location = New System.Drawing.Point(6, 159)
		Me.Label20.Name = "Label20"
		Me.Label20.Size = New System.Drawing.Size(13, 13)
		Me.Label20.TabIndex = 64
		Me.Label20.Text = "L"
		'
		'btnVisualSend
		'
		Me.btnVisualSend.Location = New System.Drawing.Point(167, 194)
		Me.btnVisualSend.Name = "btnVisualSend"
		Me.btnVisualSend.Size = New System.Drawing.Size(54, 52)
		Me.btnVisualSend.TabIndex = 63
		Me.btnVisualSend.Text = "Visual Send"
		Me.btnVisualSend.UseVisualStyleBackColor = True
		'
		'chkD7
		'
		Me.chkD7.Appearance = System.Windows.Forms.Appearance.Button
		Me.chkD7.AutoSize = True
		Me.chkD7.BackColor = System.Drawing.Color.White
		Me.chkD7.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.chkD7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.chkD7.Location = New System.Drawing.Point(130, 223)
		Me.chkD7.Name = "chkD7"
		Me.chkD7.Size = New System.Drawing.Size(31, 23)
		Me.chkD7.TabIndex = 62
		Me.chkD7.Text = "D7"
		Me.chkD7.UseVisualStyleBackColor = False
		'
		'pnl8
		'
		Me.pnl8.Controls.Add(Me.rbA8Fl)
		Me.pnl8.Controls.Add(Me.Label15)
		Me.pnl8.Controls.Add(Me.rbA8L)
		Me.pnl8.Controls.Add(Me.rbA8F)
		Me.pnl8.Controls.Add(Me.rbA8H)
		Me.pnl8.Location = New System.Drawing.Point(170, 53)
		Me.pnl8.Name = "pnl8"
		Me.pnl8.Size = New System.Drawing.Size(18, 124)
		Me.pnl8.TabIndex = 4
		'
		'rbA8Fl
		'
		Me.rbA8Fl.AutoSize = True
		Me.rbA8Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA8Fl.Enabled = False
		Me.rbA8Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA8Fl.Name = "rbA8Fl"
		Me.rbA8Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA8Fl.TabIndex = 5
		Me.rbA8Fl.UseVisualStyleBackColor = False
		'
		'Label15
		'
		Me.Label15.AutoSize = True
		Me.Label15.Location = New System.Drawing.Point(3, 3)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(13, 13)
		Me.Label15.TabIndex = 3
		Me.Label15.Text = "8"
		'
		'rbA8L
		'
		Me.rbA8L.AutoSize = True
		Me.rbA8L.Location = New System.Drawing.Point(3, 106)
		Me.rbA8L.Name = "rbA8L"
		Me.rbA8L.Size = New System.Drawing.Size(14, 13)
		Me.rbA8L.TabIndex = 2
		Me.rbA8L.UseVisualStyleBackColor = True
		'
		'rbA8F
		'
		Me.rbA8F.AutoSize = True
		Me.rbA8F.Checked = True
		Me.rbA8F.Location = New System.Drawing.Point(3, 50)
		Me.rbA8F.Name = "rbA8F"
		Me.rbA8F.Size = New System.Drawing.Size(14, 13)
		Me.rbA8F.TabIndex = 1
		Me.rbA8F.TabStop = True
		Me.rbA8F.UseVisualStyleBackColor = True
		'
		'rbA8H
		'
		Me.rbA8H.AutoSize = True
		Me.rbA8H.Location = New System.Drawing.Point(3, 22)
		Me.rbA8H.Name = "rbA8H"
		Me.rbA8H.Size = New System.Drawing.Size(14, 13)
		Me.rbA8H.TabIndex = 0
		Me.rbA8H.UseVisualStyleBackColor = True
		'
		'chkD6
		'
		Me.chkD6.Appearance = System.Windows.Forms.Appearance.Button
		Me.chkD6.AutoSize = True
		Me.chkD6.BackColor = System.Drawing.Color.White
		Me.chkD6.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.chkD6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.chkD6.Location = New System.Drawing.Point(93, 223)
		Me.chkD6.Name = "chkD6"
		Me.chkD6.Size = New System.Drawing.Size(31, 23)
		Me.chkD6.TabIndex = 61
		Me.chkD6.Text = "D6"
		Me.chkD6.UseVisualStyleBackColor = False
		'
		'pnl7
		'
		Me.pnl7.Controls.Add(Me.rbA7Fl)
		Me.pnl7.Controls.Add(Me.Label14)
		Me.pnl7.Controls.Add(Me.rbA7L)
		Me.pnl7.Controls.Add(Me.rbA7F)
		Me.pnl7.Controls.Add(Me.rbA7H)
		Me.pnl7.Location = New System.Drawing.Point(149, 53)
		Me.pnl7.Name = "pnl7"
		Me.pnl7.Size = New System.Drawing.Size(18, 124)
		Me.pnl7.TabIndex = 4
		'
		'rbA7Fl
		'
		Me.rbA7Fl.AutoSize = True
		Me.rbA7Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA7Fl.Enabled = False
		Me.rbA7Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA7Fl.Name = "rbA7Fl"
		Me.rbA7Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA7Fl.TabIndex = 5
		Me.rbA7Fl.UseVisualStyleBackColor = False
		'
		'Label14
		'
		Me.Label14.AutoSize = True
		Me.Label14.Location = New System.Drawing.Point(3, 3)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(13, 13)
		Me.Label14.TabIndex = 3
		Me.Label14.Text = "7"
		'
		'rbA7L
		'
		Me.rbA7L.AutoSize = True
		Me.rbA7L.Location = New System.Drawing.Point(3, 106)
		Me.rbA7L.Name = "rbA7L"
		Me.rbA7L.Size = New System.Drawing.Size(14, 13)
		Me.rbA7L.TabIndex = 2
		Me.rbA7L.UseVisualStyleBackColor = True
		'
		'rbA7F
		'
		Me.rbA7F.AutoSize = True
		Me.rbA7F.Checked = True
		Me.rbA7F.Location = New System.Drawing.Point(3, 50)
		Me.rbA7F.Name = "rbA7F"
		Me.rbA7F.Size = New System.Drawing.Size(14, 13)
		Me.rbA7F.TabIndex = 1
		Me.rbA7F.TabStop = True
		Me.rbA7F.UseVisualStyleBackColor = True
		'
		'rbA7H
		'
		Me.rbA7H.AutoSize = True
		Me.rbA7H.Location = New System.Drawing.Point(3, 22)
		Me.rbA7H.Name = "rbA7H"
		Me.rbA7H.Size = New System.Drawing.Size(14, 13)
		Me.rbA7H.TabIndex = 0
		Me.rbA7H.UseVisualStyleBackColor = True
		'
		'chkD5
		'
		Me.chkD5.Appearance = System.Windows.Forms.Appearance.Button
		Me.chkD5.AutoSize = True
		Me.chkD5.BackColor = System.Drawing.Color.White
		Me.chkD5.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.chkD5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.chkD5.Location = New System.Drawing.Point(56, 223)
		Me.chkD5.Name = "chkD5"
		Me.chkD5.Size = New System.Drawing.Size(31, 23)
		Me.chkD5.TabIndex = 60
		Me.chkD5.Text = "D5"
		Me.chkD5.UseVisualStyleBackColor = False
		'
		'pnl6
		'
		Me.pnl6.Controls.Add(Me.rbA6Fl)
		Me.pnl6.Controls.Add(Me.Label13)
		Me.pnl6.Controls.Add(Me.rbA6L)
		Me.pnl6.Controls.Add(Me.rbA6F)
		Me.pnl6.Controls.Add(Me.rbA6H)
		Me.pnl6.Location = New System.Drawing.Point(128, 53)
		Me.pnl6.Name = "pnl6"
		Me.pnl6.Size = New System.Drawing.Size(18, 124)
		Me.pnl6.TabIndex = 4
		'
		'rbA6Fl
		'
		Me.rbA6Fl.AutoSize = True
		Me.rbA6Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA6Fl.Enabled = False
		Me.rbA6Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA6Fl.Name = "rbA6Fl"
		Me.rbA6Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA6Fl.TabIndex = 5
		Me.rbA6Fl.UseVisualStyleBackColor = False
		'
		'Label13
		'
		Me.Label13.AutoSize = True
		Me.Label13.Location = New System.Drawing.Point(3, 3)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(13, 13)
		Me.Label13.TabIndex = 3
		Me.Label13.Text = "6"
		'
		'rbA6L
		'
		Me.rbA6L.AutoSize = True
		Me.rbA6L.Location = New System.Drawing.Point(3, 106)
		Me.rbA6L.Name = "rbA6L"
		Me.rbA6L.Size = New System.Drawing.Size(14, 13)
		Me.rbA6L.TabIndex = 2
		Me.rbA6L.UseVisualStyleBackColor = True
		'
		'rbA6F
		'
		Me.rbA6F.AutoSize = True
		Me.rbA6F.Checked = True
		Me.rbA6F.Location = New System.Drawing.Point(3, 50)
		Me.rbA6F.Name = "rbA6F"
		Me.rbA6F.Size = New System.Drawing.Size(14, 13)
		Me.rbA6F.TabIndex = 1
		Me.rbA6F.TabStop = True
		Me.rbA6F.UseVisualStyleBackColor = True
		'
		'rbA6H
		'
		Me.rbA6H.AutoSize = True
		Me.rbA6H.Location = New System.Drawing.Point(3, 22)
		Me.rbA6H.Name = "rbA6H"
		Me.rbA6H.Size = New System.Drawing.Size(14, 13)
		Me.rbA6H.TabIndex = 0
		Me.rbA6H.UseVisualStyleBackColor = True
		'
		'chkD4
		'
		Me.chkD4.Appearance = System.Windows.Forms.Appearance.Button
		Me.chkD4.AutoSize = True
		Me.chkD4.BackColor = System.Drawing.Color.White
		Me.chkD4.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.chkD4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.chkD4.Location = New System.Drawing.Point(19, 223)
		Me.chkD4.Name = "chkD4"
		Me.chkD4.Size = New System.Drawing.Size(31, 23)
		Me.chkD4.TabIndex = 59
		Me.chkD4.Text = "D4"
		Me.chkD4.UseVisualStyleBackColor = False
		'
		'pnl5
		'
		Me.pnl5.Controls.Add(Me.rbA5Fl)
		Me.pnl5.Controls.Add(Me.Label12)
		Me.pnl5.Controls.Add(Me.rbA5L)
		Me.pnl5.Controls.Add(Me.rbA5F)
		Me.pnl5.Controls.Add(Me.rbA5H)
		Me.pnl5.Location = New System.Drawing.Point(107, 53)
		Me.pnl5.Name = "pnl5"
		Me.pnl5.Size = New System.Drawing.Size(18, 124)
		Me.pnl5.TabIndex = 4
		'
		'rbA5Fl
		'
		Me.rbA5Fl.AutoSize = True
		Me.rbA5Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA5Fl.Enabled = False
		Me.rbA5Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA5Fl.Name = "rbA5Fl"
		Me.rbA5Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA5Fl.TabIndex = 5
		Me.rbA5Fl.UseVisualStyleBackColor = False
		'
		'Label12
		'
		Me.Label12.AutoSize = True
		Me.Label12.Location = New System.Drawing.Point(3, 3)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(13, 13)
		Me.Label12.TabIndex = 3
		Me.Label12.Text = "5"
		'
		'rbA5L
		'
		Me.rbA5L.AutoSize = True
		Me.rbA5L.Location = New System.Drawing.Point(3, 106)
		Me.rbA5L.Name = "rbA5L"
		Me.rbA5L.Size = New System.Drawing.Size(14, 13)
		Me.rbA5L.TabIndex = 2
		Me.rbA5L.UseVisualStyleBackColor = True
		'
		'rbA5F
		'
		Me.rbA5F.AutoSize = True
		Me.rbA5F.Checked = True
		Me.rbA5F.Location = New System.Drawing.Point(3, 50)
		Me.rbA5F.Name = "rbA5F"
		Me.rbA5F.Size = New System.Drawing.Size(14, 13)
		Me.rbA5F.TabIndex = 1
		Me.rbA5F.TabStop = True
		Me.rbA5F.UseVisualStyleBackColor = True
		'
		'rbA5H
		'
		Me.rbA5H.AutoSize = True
		Me.rbA5H.Location = New System.Drawing.Point(3, 22)
		Me.rbA5H.Name = "rbA5H"
		Me.rbA5H.Size = New System.Drawing.Size(14, 13)
		Me.rbA5H.TabIndex = 0
		Me.rbA5H.UseVisualStyleBackColor = True
		'
		'chkD3
		'
		Me.chkD3.Appearance = System.Windows.Forms.Appearance.Button
		Me.chkD3.AutoSize = True
		Me.chkD3.BackColor = System.Drawing.Color.White
		Me.chkD3.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.chkD3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.chkD3.Location = New System.Drawing.Point(130, 194)
		Me.chkD3.Name = "chkD3"
		Me.chkD3.Size = New System.Drawing.Size(31, 23)
		Me.chkD3.TabIndex = 58
		Me.chkD3.Text = "D3"
		Me.chkD3.UseVisualStyleBackColor = False
		'
		'pnl4
		'
		Me.pnl4.Controls.Add(Me.rbA4Fl)
		Me.pnl4.Controls.Add(Me.Label11)
		Me.pnl4.Controls.Add(Me.rbA4L)
		Me.pnl4.Controls.Add(Me.rbA4F)
		Me.pnl4.Controls.Add(Me.rbA4H)
		Me.pnl4.Location = New System.Drawing.Point(86, 53)
		Me.pnl4.Name = "pnl4"
		Me.pnl4.Size = New System.Drawing.Size(18, 124)
		Me.pnl4.TabIndex = 4
		'
		'rbA4Fl
		'
		Me.rbA4Fl.AutoSize = True
		Me.rbA4Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA4Fl.Enabled = False
		Me.rbA4Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA4Fl.Name = "rbA4Fl"
		Me.rbA4Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA4Fl.TabIndex = 5
		Me.rbA4Fl.UseVisualStyleBackColor = False
		'
		'Label11
		'
		Me.Label11.AutoSize = True
		Me.Label11.Location = New System.Drawing.Point(3, 3)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(13, 13)
		Me.Label11.TabIndex = 3
		Me.Label11.Text = "4"
		'
		'rbA4L
		'
		Me.rbA4L.AutoSize = True
		Me.rbA4L.Location = New System.Drawing.Point(3, 106)
		Me.rbA4L.Name = "rbA4L"
		Me.rbA4L.Size = New System.Drawing.Size(14, 13)
		Me.rbA4L.TabIndex = 2
		Me.rbA4L.UseVisualStyleBackColor = True
		'
		'rbA4F
		'
		Me.rbA4F.AutoSize = True
		Me.rbA4F.Checked = True
		Me.rbA4F.Location = New System.Drawing.Point(3, 50)
		Me.rbA4F.Name = "rbA4F"
		Me.rbA4F.Size = New System.Drawing.Size(14, 13)
		Me.rbA4F.TabIndex = 1
		Me.rbA4F.TabStop = True
		Me.rbA4F.UseVisualStyleBackColor = True
		'
		'rbA4H
		'
		Me.rbA4H.AutoSize = True
		Me.rbA4H.Location = New System.Drawing.Point(3, 22)
		Me.rbA4H.Name = "rbA4H"
		Me.rbA4H.Size = New System.Drawing.Size(14, 13)
		Me.rbA4H.TabIndex = 0
		Me.rbA4H.UseVisualStyleBackColor = True
		'
		'chkD2
		'
		Me.chkD2.Appearance = System.Windows.Forms.Appearance.Button
		Me.chkD2.AutoSize = True
		Me.chkD2.BackColor = System.Drawing.Color.White
		Me.chkD2.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.chkD2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.chkD2.Location = New System.Drawing.Point(93, 194)
		Me.chkD2.Name = "chkD2"
		Me.chkD2.Size = New System.Drawing.Size(31, 23)
		Me.chkD2.TabIndex = 57
		Me.chkD2.Text = "D2"
		Me.chkD2.UseVisualStyleBackColor = False
		'
		'pnl3
		'
		Me.pnl3.Controls.Add(Me.rbA3Fl)
		Me.pnl3.Controls.Add(Me.Label10)
		Me.pnl3.Controls.Add(Me.rbA3L)
		Me.pnl3.Controls.Add(Me.rbA3F)
		Me.pnl3.Controls.Add(Me.rbA3H)
		Me.pnl3.Location = New System.Drawing.Point(65, 53)
		Me.pnl3.Name = "pnl3"
		Me.pnl3.Size = New System.Drawing.Size(18, 124)
		Me.pnl3.TabIndex = 4
		'
		'rbA3Fl
		'
		Me.rbA3Fl.AutoSize = True
		Me.rbA3Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA3Fl.Enabled = False
		Me.rbA3Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA3Fl.Name = "rbA3Fl"
		Me.rbA3Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA3Fl.TabIndex = 5
		Me.rbA3Fl.UseVisualStyleBackColor = False
		'
		'Label10
		'
		Me.Label10.AutoSize = True
		Me.Label10.Location = New System.Drawing.Point(3, 3)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(13, 13)
		Me.Label10.TabIndex = 3
		Me.Label10.Text = "3"
		'
		'rbA3L
		'
		Me.rbA3L.AutoSize = True
		Me.rbA3L.Location = New System.Drawing.Point(3, 106)
		Me.rbA3L.Name = "rbA3L"
		Me.rbA3L.Size = New System.Drawing.Size(14, 13)
		Me.rbA3L.TabIndex = 2
		Me.rbA3L.UseVisualStyleBackColor = True
		'
		'rbA3F
		'
		Me.rbA3F.AutoSize = True
		Me.rbA3F.Checked = True
		Me.rbA3F.Location = New System.Drawing.Point(3, 50)
		Me.rbA3F.Name = "rbA3F"
		Me.rbA3F.Size = New System.Drawing.Size(14, 13)
		Me.rbA3F.TabIndex = 1
		Me.rbA3F.TabStop = True
		Me.rbA3F.UseVisualStyleBackColor = True
		'
		'rbA3H
		'
		Me.rbA3H.AutoSize = True
		Me.rbA3H.Location = New System.Drawing.Point(3, 22)
		Me.rbA3H.Name = "rbA3H"
		Me.rbA3H.Size = New System.Drawing.Size(14, 13)
		Me.rbA3H.TabIndex = 0
		Me.rbA3H.UseVisualStyleBackColor = True
		'
		'chkD1
		'
		Me.chkD1.Appearance = System.Windows.Forms.Appearance.Button
		Me.chkD1.AutoSize = True
		Me.chkD1.BackColor = System.Drawing.Color.White
		Me.chkD1.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.chkD1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.chkD1.Location = New System.Drawing.Point(56, 194)
		Me.chkD1.Name = "chkD1"
		Me.chkD1.Size = New System.Drawing.Size(31, 23)
		Me.chkD1.TabIndex = 56
		Me.chkD1.Text = "D1"
		Me.chkD1.UseVisualStyleBackColor = False
		'
		'pnl2
		'
		Me.pnl2.Controls.Add(Me.rbA2Fl)
		Me.pnl2.Controls.Add(Me.Label9)
		Me.pnl2.Controls.Add(Me.rbA2L)
		Me.pnl2.Controls.Add(Me.rbA2F)
		Me.pnl2.Controls.Add(Me.rbA2H)
		Me.pnl2.Location = New System.Drawing.Point(44, 53)
		Me.pnl2.Name = "pnl2"
		Me.pnl2.Size = New System.Drawing.Size(18, 124)
		Me.pnl2.TabIndex = 4
		'
		'rbA2Fl
		'
		Me.rbA2Fl.AutoSize = True
		Me.rbA2Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA2Fl.Enabled = False
		Me.rbA2Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA2Fl.Name = "rbA2Fl"
		Me.rbA2Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA2Fl.TabIndex = 5
		Me.rbA2Fl.UseVisualStyleBackColor = False
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Location = New System.Drawing.Point(3, 3)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(13, 13)
		Me.Label9.TabIndex = 3
		Me.Label9.Text = "2"
		'
		'rbA2L
		'
		Me.rbA2L.AutoSize = True
		Me.rbA2L.Location = New System.Drawing.Point(3, 106)
		Me.rbA2L.Name = "rbA2L"
		Me.rbA2L.Size = New System.Drawing.Size(14, 13)
		Me.rbA2L.TabIndex = 2
		Me.rbA2L.UseVisualStyleBackColor = True
		'
		'rbA2F
		'
		Me.rbA2F.AutoSize = True
		Me.rbA2F.Checked = True
		Me.rbA2F.Location = New System.Drawing.Point(3, 50)
		Me.rbA2F.Name = "rbA2F"
		Me.rbA2F.Size = New System.Drawing.Size(14, 13)
		Me.rbA2F.TabIndex = 1
		Me.rbA2F.TabStop = True
		Me.rbA2F.UseVisualStyleBackColor = True
		'
		'rbA2H
		'
		Me.rbA2H.AutoSize = True
		Me.rbA2H.Location = New System.Drawing.Point(3, 22)
		Me.rbA2H.Name = "rbA2H"
		Me.rbA2H.Size = New System.Drawing.Size(14, 13)
		Me.rbA2H.TabIndex = 0
		Me.rbA2H.UseVisualStyleBackColor = True
		'
		'chkD0
		'
		Me.chkD0.Appearance = System.Windows.Forms.Appearance.Button
		Me.chkD0.AutoSize = True
		Me.chkD0.BackColor = System.Drawing.Color.White
		Me.chkD0.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
		Me.chkD0.FlatStyle = System.Windows.Forms.FlatStyle.Flat
		Me.chkD0.Location = New System.Drawing.Point(19, 194)
		Me.chkD0.Name = "chkD0"
		Me.chkD0.Size = New System.Drawing.Size(31, 23)
		Me.chkD0.TabIndex = 55
		Me.chkD0.Text = "D0"
		Me.chkD0.ThreeState = True
		Me.chkD0.UseVisualStyleBackColor = False
		'
		'pnl1
		'
		Me.pnl1.Controls.Add(Me.rbA1Fl)
		Me.pnl1.Controls.Add(Me.Label8)
		Me.pnl1.Controls.Add(Me.rbA1L)
		Me.pnl1.Controls.Add(Me.rbA1F)
		Me.pnl1.Controls.Add(Me.rbA1H)
		Me.pnl1.Location = New System.Drawing.Point(23, 53)
		Me.pnl1.Name = "pnl1"
		Me.pnl1.Size = New System.Drawing.Size(18, 124)
		Me.pnl1.TabIndex = 3
		'
		'rbA1Fl
		'
		Me.rbA1Fl.AutoSize = True
		Me.rbA1Fl.BackColor = System.Drawing.SystemColors.Control
		Me.rbA1Fl.Enabled = False
		Me.rbA1Fl.Location = New System.Drawing.Point(3, 78)
		Me.rbA1Fl.Name = "rbA1Fl"
		Me.rbA1Fl.Size = New System.Drawing.Size(14, 13)
		Me.rbA1Fl.TabIndex = 4
		Me.rbA1Fl.UseVisualStyleBackColor = False
		'
		'Label8
		'
		Me.Label8.AutoSize = True
		Me.Label8.Location = New System.Drawing.Point(3, 3)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(13, 13)
		Me.Label8.TabIndex = 3
		Me.Label8.Text = "1"
		'
		'rbA1L
		'
		Me.rbA1L.AutoSize = True
		Me.rbA1L.Location = New System.Drawing.Point(3, 106)
		Me.rbA1L.Name = "rbA1L"
		Me.rbA1L.Size = New System.Drawing.Size(14, 13)
		Me.rbA1L.TabIndex = 2
		Me.rbA1L.UseVisualStyleBackColor = True
		'
		'rbA1F
		'
		Me.rbA1F.AutoSize = True
		Me.rbA1F.Checked = True
		Me.rbA1F.Location = New System.Drawing.Point(3, 50)
		Me.rbA1F.Name = "rbA1F"
		Me.rbA1F.Size = New System.Drawing.Size(14, 13)
		Me.rbA1F.TabIndex = 1
		Me.rbA1F.TabStop = True
		Me.rbA1F.UseVisualStyleBackColor = True
		'
		'rbA1H
		'
		Me.rbA1H.AutoSize = True
		Me.rbA1H.Location = New System.Drawing.Point(3, 22)
		Me.rbA1H.Name = "rbA1H"
		Me.rbA1H.Size = New System.Drawing.Size(14, 13)
		Me.rbA1H.TabIndex = 0
		Me.rbA1H.UseVisualStyleBackColor = True
		'
		'Label7
		'
		Me.Label7.AutoSize = True
		Me.Label7.Location = New System.Drawing.Point(6, 103)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(13, 13)
		Me.Label7.TabIndex = 2
		Me.Label7.Text = "F"
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.ForeColor = System.Drawing.Color.Red
		Me.Label6.Location = New System.Drawing.Point(6, 131)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(14, 13)
		Me.Label6.TabIndex = 1
		Me.Label6.Text = "●"
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Location = New System.Drawing.Point(6, 75)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(15, 13)
		Me.Label5.TabIndex = 0
		Me.Label5.Text = "H"
		'
		'txtHexSend
		'
		Me.txtHexSend.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtHexSend.Location = New System.Drawing.Point(36, 25)
		Me.txtHexSend.MaxLength = 6
		Me.txtHexSend.Name = "txtHexSend"
		Me.txtHexSend.Size = New System.Drawing.Size(76, 26)
		Me.txtHexSend.TabIndex = 64
		Me.txtHexSend.Text = "00 00 00"
		Me.txtHexSend.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		'
		'Label16
		'
		Me.Label16.AutoSize = True
		Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label16.Location = New System.Drawing.Point(9, 28)
		Me.Label16.Name = "Label16"
		Me.Label16.Size = New System.Drawing.Size(25, 20)
		Me.Label16.TabIndex = 65
		Me.Label16.Text = "0x"
		Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'grpHexSend
		'
		Me.grpHexSend.Controls.Add(Me.btnHexClear)
		Me.grpHexSend.Controls.Add(Me.btnHexSend)
		Me.grpHexSend.Controls.Add(Me.Label16)
		Me.grpHexSend.Controls.Add(Me.txtHexSend)
		Me.grpHexSend.Location = New System.Drawing.Point(447, 386)
		Me.grpHexSend.Name = "grpHexSend"
		Me.grpHexSend.Size = New System.Drawing.Size(239, 65)
		Me.grpHexSend.TabIndex = 66
		Me.grpHexSend.TabStop = False
		Me.grpHexSend.Text = "Hexadecimal Send"
		'
		'btnHexClear
		'
		Me.btnHexClear.Location = New System.Drawing.Point(118, 25)
		Me.btnHexClear.Name = "btnHexClear"
		Me.btnHexClear.Size = New System.Drawing.Size(41, 26)
		Me.btnHexClear.TabIndex = 67
		Me.btnHexClear.Text = "Clear"
		Me.btnHexClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.btnHexClear.UseVisualStyleBackColor = True
		'
		'btnHexSend
		'
		Me.btnHexSend.Location = New System.Drawing.Point(165, 25)
		Me.btnHexSend.Name = "btnHexSend"
		Me.btnHexSend.Size = New System.Drawing.Size(62, 26)
		Me.btnHexSend.TabIndex = 66
		Me.btnHexSend.Text = "Hex Send"
		Me.btnHexSend.UseVisualStyleBackColor = True
		'
		'grpCfg
		'
		Me.grpCfg.BackColor = System.Drawing.SystemColors.Control
		Me.grpCfg.Controls.Add(Me.Label19)
		Me.grpCfg.Controls.Add(Me.txtNumPulses)
		Me.grpCfg.Controls.Add(Me.Label18)
		Me.grpCfg.Controls.Add(Me.rd32mS)
		Me.grpCfg.Controls.Add(Me.rd16mS)
		Me.grpCfg.Controls.Add(Me.rd48mS)
		Me.grpCfg.Controls.Add(Me.rd64mS)
		Me.grpCfg.Location = New System.Drawing.Point(449, 12)
		Me.grpCfg.Name = "grpCfg"
		Me.grpCfg.Size = New System.Drawing.Size(238, 99)
		Me.grpCfg.TabIndex = 67
		Me.grpCfg.TabStop = False
		Me.grpCfg.Text = "Send Data Config"
		'
		'Label19
		'
		Me.Label19.AutoSize = True
		Me.Label19.Location = New System.Drawing.Point(154, 24)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(58, 13)
		Me.Label19.TabIndex = 75
		Me.Label19.Text = "No. Pulses"
		'
		'txtNumPulses
		'
		Me.txtNumPulses.Location = New System.Drawing.Point(157, 54)
		Me.txtNumPulses.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
		Me.txtNumPulses.Name = "txtNumPulses"
		Me.txtNumPulses.Size = New System.Drawing.Size(55, 20)
		Me.txtNumPulses.TabIndex = 74
		Me.txtNumPulses.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
		Me.txtNumPulses.Value = New Decimal(New Integer() {10, 0, 0, 0})
		'
		'Label18
		'
		Me.Label18.AutoSize = True
		Me.Label18.Location = New System.Drawing.Point(34, 24)
		Me.Label18.Name = "Label18"
		Me.Label18.Size = New System.Drawing.Size(76, 13)
		Me.Label18.TabIndex = 73
		Me.Label18.Text = "Pulse Duration"
		'
		'rd32mS
		'
		Me.rd32mS.AutoSize = True
		Me.rd32mS.Checked = True
		Me.rd32mS.Location = New System.Drawing.Point(80, 44)
		Me.rd32mS.Name = "rd32mS"
		Me.rd32mS.Size = New System.Drawing.Size(52, 17)
		Me.rd32mS.TabIndex = 72
		Me.rd32mS.TabStop = True
		Me.rd32mS.Text = "32mS"
		Me.rd32mS.UseVisualStyleBackColor = True
		'
		'rd16mS
		'
		Me.rd16mS.AutoSize = True
		Me.rd16mS.Location = New System.Drawing.Point(12, 44)
		Me.rd16mS.Name = "rd16mS"
		Me.rd16mS.Size = New System.Drawing.Size(52, 17)
		Me.rd16mS.TabIndex = 71
		Me.rd16mS.TabStop = True
		Me.rd16mS.Text = "16mS"
		Me.rd16mS.UseVisualStyleBackColor = True
		'
		'rd48mS
		'
		Me.rd48mS.AutoSize = True
		Me.rd48mS.Location = New System.Drawing.Point(12, 67)
		Me.rd48mS.Name = "rd48mS"
		Me.rd48mS.Size = New System.Drawing.Size(52, 17)
		Me.rd48mS.TabIndex = 70
		Me.rd48mS.Text = "48mS"
		Me.rd48mS.UseVisualStyleBackColor = True
		'
		'rd64mS
		'
		Me.rd64mS.AutoSize = True
		Me.rd64mS.Location = New System.Drawing.Point(80, 67)
		Me.rd64mS.Name = "rd64mS"
		Me.rd64mS.Size = New System.Drawing.Size(52, 17)
		Me.rd64mS.TabIndex = 69
		Me.rd64mS.TabStop = True
		Me.rd64mS.Text = "64mS"
		Me.rd64mS.UseVisualStyleBackColor = True
		'
		'frmMain
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(700, 576)
		Me.Controls.Add(Me.grpCfg)
		Me.Controls.Add(Me.grpHexSend)
		Me.Controls.Add(Me.grpVisualSend)
		Me.Controls.Add(Me.grpAddress)
		Me.Controls.Add(Me.grpChannels)
		Me.Controls.Add(Me.grpTiming)
		Me.Controls.Add(Me.grpData)
		Me.Controls.Add(Me.grpCommunications)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.grpButtons)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.HelpButton = True
		Me.Location = New System.Drawing.Point(50, 100)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmMain"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "RF Analyzer"
		Me.grpButtons.ResumeLayout(False)
		Me.GroupBox2.ResumeLayout(False)
		Me.grpCommunications.ResumeLayout(False)
		Me.grpCommunications.PerformLayout()
		Me.grpData.ResumeLayout(False)
		Me.grpData.PerformLayout()
		Me.grpAddress.ResumeLayout(False)
		Me.grpAddress.PerformLayout()
		Me.grpTiming.ResumeLayout(False)
		Me.grpTiming.PerformLayout()
		Me.grpChannels.ResumeLayout(False)
		Me.grpChannels.PerformLayout()
		Me.grpVisualSend.ResumeLayout(False)
		Me.grpVisualSend.PerformLayout()
		Me.pnl10.ResumeLayout(False)
		Me.pnl10.PerformLayout()
		Me.pnl9.ResumeLayout(False)
		Me.pnl9.PerformLayout()
		Me.pnl8.ResumeLayout(False)
		Me.pnl8.PerformLayout()
		Me.pnl7.ResumeLayout(False)
		Me.pnl7.PerformLayout()
		Me.pnl6.ResumeLayout(False)
		Me.pnl6.PerformLayout()
		Me.pnl5.ResumeLayout(False)
		Me.pnl5.PerformLayout()
		Me.pnl4.ResumeLayout(False)
		Me.pnl4.PerformLayout()
		Me.pnl3.ResumeLayout(False)
		Me.pnl3.PerformLayout()
		Me.pnl2.ResumeLayout(False)
		Me.pnl2.PerformLayout()
		Me.pnl1.ResumeLayout(False)
		Me.pnl1.PerformLayout()
		Me.grpHexSend.ResumeLayout(False)
		Me.grpHexSend.PerformLayout()
		Me.grpCfg.ResumeLayout(False)
		Me.grpCfg.PerformLayout()
		CType(Me.txtNumPulses, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents btnAbrir As System.Windows.Forms.Button
    Friend WithEvents cmbPuertos As System.Windows.Forms.ComboBox
    Friend WithEvents Com As System.IO.Ports.SerialPort
    Friend WithEvents txtComm As System.Windows.Forms.TextBox
    Friend WithEvents btnCerrar As System.Windows.Forms.Button
    Friend WithEvents btnLimpiar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnD0 As System.Windows.Forms.Button
    Friend WithEvents btnD1 As System.Windows.Forms.Button
    Friend WithEvents btnD2 As System.Windows.Forms.Button
    Friend WithEvents btnD3 As System.Windows.Forms.Button
    Friend WithEvents lbAddr5 As System.Windows.Forms.Label
    Friend WithEvents lbAddr6 As System.Windows.Forms.Label
    Friend WithEvents lbAddr7 As System.Windows.Forms.Label
    Friend WithEvents lbAddr8 As System.Windows.Forms.Label
    Friend WithEvents lbAddr3 As System.Windows.Forms.Label
    Friend WithEvents lbAddr4 As System.Windows.Forms.Label
    Friend WithEvents lbAddr1 As System.Windows.Forms.Label
    Friend WithEvents lbAddr2 As System.Windows.Forms.Label
    Friend WithEvents grpButtons As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents grpCommunications As System.Windows.Forms.GroupBox
    Friend WithEvents grpData As System.Windows.Forms.GroupBox
    Friend WithEvents lbBtn As System.Windows.Forms.Label
    Friend WithEvents lbAddr As System.Windows.Forms.Label
    Friend WithEvents lbTotTime As System.Windows.Forms.Label
    Friend WithEvents lbRcvDat As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lbCode As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents grpTiming As System.Windows.Forms.GroupBox
    Friend WithEvents lbSyncTime As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents lbBitTime As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbOscClk As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents btnD7 As System.Windows.Forms.Button
    Friend WithEvents btnD4 As System.Windows.Forms.Button
    Friend WithEvents btnD5 As System.Windows.Forms.Button
    Friend WithEvents btnD6 As System.Windows.Forms.Button
    Friend WithEvents lbPattern As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents lbOrder As System.Windows.Forms.Label
    Friend WithEvents grpChannels As System.Windows.Forms.GroupBox
    Friend WithEvents rb8CH As System.Windows.Forms.RadioButton
    Friend WithEvents rb6CH As System.Windows.Forms.RadioButton
    Friend WithEvents rb4CH As System.Windows.Forms.RadioButton
    Friend WithEvents grpAddress As System.Windows.Forms.GroupBox
    Friend WithEvents chkInvPins As System.Windows.Forms.CheckBox
    Friend WithEvents chkInvHL As System.Windows.Forms.CheckBox
    Friend WithEvents lbAddrH As System.Windows.Forms.Label
    Friend WithEvents lbAddrL As System.Windows.Forms.Label
    Friend WithEvents grpVisualSend As System.Windows.Forms.GroupBox
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents rbA1L As System.Windows.Forms.RadioButton
    Friend WithEvents rbA1F As System.Windows.Forms.RadioButton
    Friend WithEvents rbA1H As System.Windows.Forms.RadioButton
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents pnl8 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents rbA8L As System.Windows.Forms.RadioButton
    Friend WithEvents rbA8F As System.Windows.Forms.RadioButton
    Friend WithEvents rbA8H As System.Windows.Forms.RadioButton
    Friend WithEvents pnl7 As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents rbA7L As System.Windows.Forms.RadioButton
    Friend WithEvents rbA7F As System.Windows.Forms.RadioButton
    Friend WithEvents rbA7H As System.Windows.Forms.RadioButton
    Friend WithEvents pnl6 As System.Windows.Forms.Panel
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents rbA6L As System.Windows.Forms.RadioButton
    Friend WithEvents rbA6F As System.Windows.Forms.RadioButton
    Friend WithEvents rbA6H As System.Windows.Forms.RadioButton
    Friend WithEvents pnl5 As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents rbA5L As System.Windows.Forms.RadioButton
    Friend WithEvents rbA5F As System.Windows.Forms.RadioButton
    Friend WithEvents rbA5H As System.Windows.Forms.RadioButton
    Friend WithEvents pnl4 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents rbA4L As System.Windows.Forms.RadioButton
    Friend WithEvents rbA4F As System.Windows.Forms.RadioButton
    Friend WithEvents rbA4H As System.Windows.Forms.RadioButton
    Friend WithEvents pnl3 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents rbA3L As System.Windows.Forms.RadioButton
    Friend WithEvents rbA3F As System.Windows.Forms.RadioButton
    Friend WithEvents rbA3H As System.Windows.Forms.RadioButton
    Friend WithEvents pnl2 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents rbA2L As System.Windows.Forms.RadioButton
    Friend WithEvents rbA2F As System.Windows.Forms.RadioButton
    Friend WithEvents rbA2H As System.Windows.Forms.RadioButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents chkD0 As System.Windows.Forms.CheckBox
    Friend WithEvents chkD1 As System.Windows.Forms.CheckBox
    Friend WithEvents chkD2 As System.Windows.Forms.CheckBox
    Friend WithEvents chkD3 As System.Windows.Forms.CheckBox
    Friend WithEvents chkD4 As System.Windows.Forms.CheckBox
    Friend WithEvents chkD5 As System.Windows.Forms.CheckBox
    Friend WithEvents chkD6 As System.Windows.Forms.CheckBox
    Friend WithEvents chkD7 As System.Windows.Forms.CheckBox
    Friend WithEvents txtHexSend As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents grpHexSend As System.Windows.Forms.GroupBox
    Friend WithEvents btnHexSend As System.Windows.Forms.Button
    Friend WithEvents btnVisualSend As System.Windows.Forms.Button
    Friend WithEvents chk8CHinv As CheckBox
    Friend WithEvents btnHexClear As Button
    Friend WithEvents grpCfg As GroupBox
    Friend WithEvents lbTBS As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents txtNumPulses As NumericUpDown
    Friend WithEvents Label18 As Label
    Friend WithEvents rd32mS As RadioButton
    Friend WithEvents rd16mS As RadioButton
    Friend WithEvents rd48mS As RadioButton
    Friend WithEvents rd64mS As RadioButton
    Friend WithEvents rbA1Fl As RadioButton
    Friend WithEvents Label20 As Label
    Friend WithEvents rbA8Fl As RadioButton
    Friend WithEvents rbA7Fl As RadioButton
    Friend WithEvents rbA6Fl As RadioButton
    Friend WithEvents rbA5Fl As RadioButton
    Friend WithEvents rbA4Fl As RadioButton
    Friend WithEvents rbA3Fl As RadioButton
    Friend WithEvents rbA2Fl As RadioButton
    Friend WithEvents chkLearning As CheckBox
	Friend WithEvents lbHexVisualSend As Label
	Friend WithEvents Label21 As Label
	Friend WithEvents pnl10 As Panel
	Friend WithEvents rbA10Fl As RadioButton
	Friend WithEvents Label28 As Label
	Friend WithEvents rbA10L As RadioButton
	Friend WithEvents rbA10F As RadioButton
	Friend WithEvents rbA10H As RadioButton
	Friend WithEvents pnl9 As Panel
	Friend WithEvents rbA9Fl As RadioButton
	Friend WithEvents Label22 As Label
	Friend WithEvents rbA9L As RadioButton
	Friend WithEvents rbA9F As RadioButton
	Friend WithEvents rbA9H As RadioButton
	Friend WithEvents lbAddr10 As Label
	Friend WithEvents lbAddr9 As Label
	Friend WithEvents lbCode2 As Label
End Class
