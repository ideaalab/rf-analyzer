﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHelp
	Inherits System.Windows.Forms.Form

	'Form reemplaza a Dispose para limpiar la lista de componentes.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Requerido por el Diseñador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	'Se puede modificar usando el Diseñador de Windows Forms.  
	'No lo modifique con el editor de código.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.rtbHelp = New System.Windows.Forms.RichTextBox()
		Me.SuspendLayout()
		'
		'rtbHelp
		'
		Me.rtbHelp.Location = New System.Drawing.Point(12, 12)
		Me.rtbHelp.Name = "rtbHelp"
		Me.rtbHelp.ReadOnly = True
		Me.rtbHelp.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
		Me.rtbHelp.Size = New System.Drawing.Size(694, 455)
		Me.rtbHelp.TabIndex = 0
		Me.rtbHelp.Text = "" & Global.Microsoft.VisualBasic.ChrW(10)
		'
		'frmHelp
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(718, 479)
		Me.Controls.Add(Me.rtbHelp)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmHelp"
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Help"
		Me.TopMost = True
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents rtbHelp As RichTextBox
End Class
