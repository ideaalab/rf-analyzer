﻿'Imports System.Diagnostics

Imports System.ComponentModel

Public Class frmMain
	Dim Learning As Boolean         'tells if the remote is fixed of learning code
	Dim Buttons As Byte             'keeps the value of the buttons pressed
	Dim ButtonsLo As Byte           'keeps the low value of the butons pressed for 6ch remote
	Dim AddressHi As Byte           'keeps the address of the remote
	Dim AddressMi As Byte           'keeps the address of the remote
	Dim AddressLo As Byte           'keeps the address of the remote

	Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Puertos()
		Activar()
		GetVisualValue()

		Call btnAbrir_Click(Nothing, Nothing)

		CheckForIllegalCrossThreadCalls = False

	End Sub

	Private Sub Puertos()
		cmbPuertos.Items.Clear()    'borra

		' Show all available COM ports.
		For Each sp As String In My.Computer.Ports.SerialPortNames
			cmbPuertos.Items.Add(sp)
		Next

		If cmbPuertos.Items.Count > 0 Then
			cmbPuertos.SelectedIndex = 0
		End If
	End Sub

	Private Sub btnAbrir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAbrir.Click

		Try
			'Com = My.Computer.Ports.OpenSerialPort(cmbPuertos.Text, 19200, IO.Ports.Parity.None, 8, 1)

			Com.Close()
			Com.PortName = cmbPuertos.Text
			'Com.BaudRate = 19200
			'Com.BaudRate = 115200
			Com.BaudRate = 250000
			Com.Parity = IO.Ports.Parity.None
			Com.DataBits = 8
			Com.StopBits = 1
			Com.ReadTimeout = 1000
			Com.Open()

			Com.DiscardInBuffer()   'discard buffer

		Catch ex As TimeoutException
			Debug.Print("Timeout Exception")
		Catch ex As InvalidCastException
			Debug.Print("Invalid Cast Exception")
		Catch ex As OverflowException
			Debug.Print("Overflow Exception")
		Catch
			Debug.Print("Excepcion no controlada")
			Call btnCerrar_Click()
			MsgBox("No se pudo abrir el puerto.", MsgBoxStyle.Exclamation)
		End Try

		Call Activar()  'enable or disable controls whether if the port was open

	End Sub

	Private Sub btnCerrar_Click() Handles btnCerrar.Click
		Com.Close()
		Call Activar()  'disable all controls
		Call Puertos()  'check for available ports
	End Sub

	Private Sub Activar()
		btnAbrir.Enabled = Not Com.IsOpen
		cmbPuertos.Enabled = Not Com.IsOpen
		btnCerrar.Enabled = Com.IsOpen

		grpCommunications.Enabled = Com.IsOpen
		grpChannels.Enabled = Com.IsOpen
		grpData.Enabled = Com.IsOpen
		grpTiming.Enabled = Com.IsOpen
		grpButtons.Enabled = Com.IsOpen
		grpAddress.Enabled = Com.IsOpen
		grpCfg.Enabled = Com.IsOpen
		grpVisualSend.Enabled = Com.IsOpen
		grpHexSend.Enabled = Com.IsOpen

	End Sub

	Private Sub btnLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
		txtComm.Text = ""

	End Sub

	Private Sub Com_DataReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles Com.DataReceived
		Dim x As Integer

		'--- read COM port ---
		Try
			While Com.BytesToRead > 0
				'move all bytes one position down
				For x = 0 To (RX_BUFF_SIZE - 1) Step 1
					RxBuff(x) = RxBuff(x + 1)
				Next

				'save received byte on last position
				RxBuff(RX_BUFF_SIZE - 1) = Com.ReadByte
				Debug.Print("0x" & Hex(RxBuff(RX_BUFF_SIZE - 1)).PadLeft(2, "0"))   'show received on console

				'if first two bytes match header then decode data
				If (RxBuff(0) = HEADER1) And (RxBuff(1) = HEADER2) Then
					Dim Received As String
					'------ print on textbox -----------
					Received = "Received: "

					For x = 0 To (RX_BUFF_SIZE - 1) Step 1
						Received = Received & "0x" & Hex(RxBuff(x)).PadLeft(2, "0"c) & " "
					Next

					txtComm.Text = Received & vbCrLf & vbCrLf & txtComm.Text
					'-----------------------------------

					Call DecodeData()
				End If

			End While

		Catch ex As TimeoutException
			Debug.Print("Timeout Exception")
		Catch ex As InvalidCastException
			Debug.Print("Invalid Cast Exception")
		Catch ex As OverflowException
			Debug.Print("Overflow Exception")
		Catch
			Debug.Print("Excepcion no controlada")
			'Call btnCerrar_Click()
		End Try
		'------------------
	End Sub

	Private Sub DecodeData()
		Dim State As Integer

		'show reecived data in bit and hex format
		lbRcvDat.Text = "0b " & Convert.ToString(RxBuff(2), 2).PadLeft(8, "0"c) & " " & Convert.ToString(RxBuff(3), 2).PadLeft(8, "0"c) & " " & Convert.ToString(RxBuff(4), 2).PadLeft(8, "0"c) & " (0x " & Hex(RxBuff(2)).PadLeft(2, "0"c) & " " & Hex(RxBuff(3)).PadLeft(2, "0"c) & " " & Hex(RxBuff(4)).PadLeft(2, "0"c) & ")"

		Learning = False

		'check if any bit pair is &B01, as only learning code remotes use this pair
		For ByteNum As Byte = 0 To 2 Step 1
			For Index As Byte = 0 To 6 Step 2
				State = (RxBuff(ByteNum + 2) >> Index) And 3

				If State = LEARNING_FLOAT Then
					Learning = True
					Exit For
				End If
			Next

			If Learning = True Then
				Exit For
			End If
		Next

		If Learning = True Then
			Buttons = RxBuff(2) >> 4

			lbOrder.Text = "     | Btn |              Address              |"
			lbPattern.Text = "D3 ~ D0 / A19 ~ A0 <- LSB sent first"
			lbCode.Text = "Learning"
			lbCode2.Text = "(4 bits buttons / 20 bits address)"
		Else
			Buttons = RxBuff(2)

			lbCode.Text = "Fixed"

			If rb4CH.Checked = True Then
				lbOrder.Text = "     |     Btn     |          Address          |"
				lbPattern.Text = "D0 ~ D3 / A7 ~ A0 <- LSB sent first"
				lbCode2.Text = "(8 bits buttons / 16 bits address)"
			End If

			If rb6CH.Checked = True Then
				lbOrder.Text = "     |         Btn          |     Address      |"
				lbPattern.Text = "D0 ~ D5 / A5 ~ A0 <- LSB sent first"
				lbCode2.Text = "(12 bits buttons / 12 bits address)"

				ButtonsLo = RxBuff(3) >> 4
			End If

			If rb8CH.Checked = True Then
				lbOrder.Text = "     |     Btn     |          Address          |"
				lbPattern.Text = "Data / A7 ~ A0 <- LSB sent first"
				lbCode2.Text = "(8 bits buttons / 16 bits address)"
			End If

		End If

		lbCode2.Left = lbCode.Left + lbCode.Width

		'BUTTON DECODING -------------------------

		'reset values
		btnD7.BackColor = Color.White
		btnD6.BackColor = Color.White
		btnD5.BackColor = Color.White
		btnD4.BackColor = Color.White
		btnD3.BackColor = Color.White
		btnD2.BackColor = Color.White
		btnD1.BackColor = Color.White
		btnD0.BackColor = Color.White

		'--- decode fixed code button ---
		If Learning = False Then
			'show visual representation of 4 and 6ch buttons
			If rb4CH.Checked = True Or rb6CH.Checked = True Then

				'high byte
				For Index As Byte = 0 To 6 Step 2
					State = (Buttons >> Index) And 3

					Select Case Index
						Case 0
							If State = FIXED_FLOAT Then        'float
								btnD3.BackColor = Color.Gray
							ElseIf State = HIGH Then            'high
								btnD3.BackColor = Color.Green
							End If
						Case 2
							If State = FIXED_FLOAT Then        'float
								btnD2.BackColor = Color.Gray
							ElseIf State = HIGH Then            'high
								btnD2.BackColor = Color.Green
							End If
						Case 4
							If State = FIXED_FLOAT Then        'float
								btnD1.BackColor = Color.Gray
							ElseIf State = HIGH Then            'high
								btnD1.BackColor = Color.Green
							End If
						Case 6
							If State = FIXED_FLOAT Then        'float
								btnD0.BackColor = Color.Gray
							ElseIf State = HIGH Then            'high
								btnD0.BackColor = Color.Green
							End If
					End Select
				Next

				If rb4CH.Checked = True Then
					lbBtn.Text = "0b " & Convert.ToString(Buttons, 2).PadLeft(8, "0"c) & " (0x " & Hex(Buttons).PadLeft(2, "0"c) & ")"
				Else
					lbBtn.Text = "0b " & Convert.ToString(Buttons, 2).PadLeft(8, "0"c) & " " & Convert.ToString(ButtonsLo, 2).PadLeft(4, "0"c) & " (0x " & Hex(Buttons).PadLeft(2, "0"c) & " " & Hex(ButtonsLo).PadLeft(1, "0"c) & ")"

					'4 MSb of mid byte
					For Index As Byte = 0 To 2 Step 2
						State = (ButtonsLo >> Index) And 3

						Select Case Index
							Case 0
								If State = FIXED_FLOAT Then        'float
									btnD5.BackColor = Color.Gray
								ElseIf State = HIGH Then            'high
									btnD5.BackColor = Color.Green
								End If
							Case 2
								If State = FIXED_FLOAT Then        'float
									btnD4.BackColor = Color.Gray
								ElseIf State = HIGH Then            'high
									btnD4.BackColor = Color.Green
								End If
						End Select
					Next
				End If

			Else    'show visual representation of 8ch buttons
				Dim Val8Ch = 0

				lbBtn.Text = "0b " & Convert.ToString(Buttons, 2).PadLeft(8, "0"c) & " (0x " & Hex(Buttons).PadLeft(2, "0"c) & ")"

				For Index As Byte = 0 To 6 Step 2
					State = (Buttons >> Index) And 3

					If State = HIGH Then
						If chk8CHinv.Checked = False Then

							Select Case Index
								Case 0
									Val8Ch = Val8Ch + 8
								Case 2
									Val8Ch = Val8Ch + 4
								Case 4
									Val8Ch = Val8Ch + 2
								Case 6
									Val8Ch = Val8Ch + 1
							End Select
						Else
							Select Case Index
								Case 0
									Val8Ch = Val8Ch + 1
								Case 2
									Val8Ch = Val8Ch + 2
								Case 4
									Val8Ch = Val8Ch + 4
								Case 6
									Val8Ch = Val8Ch + 8
							End Select
						End If
					End If

				Next

				Select Case Val8Ch
					Case 1
						btnD0.BackColor = Color.Green
					Case 2
						btnD1.BackColor = Color.Green
					Case 3
						btnD2.BackColor = Color.Green
					Case 4
						btnD3.BackColor = Color.Green
					Case 5
						btnD4.BackColor = Color.Green
					Case 6
						btnD5.BackColor = Color.Green
					Case 7
						btnD6.BackColor = Color.Green
					Case 8
						btnD7.BackColor = Color.Green
				End Select
			End If

			'--- decode learning code buttons ---
		Else
			lbBtn.Text = "0b " & Convert.ToString(Buttons, 2).PadLeft(4, "0"c) & " (0x " & Hex(Buttons) & ")"

			Select Case Buttons
				Case 1
					btnD0.BackColor = Color.Green
				Case 2
					btnD1.BackColor = Color.Green
				Case 4
					btnD2.BackColor = Color.Green
				Case 8
					btnD3.BackColor = Color.Green
			End Select
		End If

		'ADDRESS DECODING -------------------------
		AddressHi = RxBuff(2) And 15    'only used if its learning remote

		If rb6CH.Checked = False Then
			AddressMi = RxBuff(3)
		Else
			AddressMi = RxBuff(3) And 15    'address of 6ch only use lower part of mid byte
		End If

		AddressLo = RxBuff(4)

		If Learning = False Then    '12 or 16 bits wide
			If rb6CH.Checked = True Then    '6ch remote / 12 bits wide address
				'binary part
				lbAddr.Text = "0b " & Convert.ToString(AddressMi, 2).PadLeft(4, "0"c) & " " & Convert.ToString(AddressLo, 2).PadLeft(8, "0"c)
				'hex part
				lbAddr.Text = lbAddr.Text & " (0x " & Hex(AddressMi).PadLeft(1, "0"c) & " " & Hex(AddressLo).PadLeft(2, "0"c) & ")"
			Else    '4ch or 8ch remote / 16 bits wide address
				'binary part
				lbAddr.Text = "0b " & Convert.ToString(AddressMi, 2).PadLeft(8, "0"c) & " " & Convert.ToString(AddressLo, 2).PadLeft(8, "0"c)
				'hex part
				lbAddr.Text = lbAddr.Text & " (0x " & Hex(AddressMi).PadLeft(2, "0"c) & " " & Hex(AddressLo).PadLeft(2, "0"c) & ")"
			End If

		Else    '20 bits wide
			'binary part
			lbAddr.Text = "0b " & Convert.ToString(AddressHi, 2).PadLeft(4, "0"c) & " " & Convert.ToString(AddressMi, 2).PadLeft(8, "0"c) & " " & Convert.ToString(AddressLo, 2).PadLeft(8, "0"c)
			'hex part
			lbAddr.Text = lbAddr.Text & " (0x " & Hex(AddressHi) & " " & Hex(AddressMi).PadLeft(2, "0"c) & " " & Hex(AddressLo).PadLeft(2, "0"c) & ")"

			'if its learning, can only be 4ch remote
			rb4CH.Checked = True
		End If

		'SHOW CODE ADDRESS -----------------------
		grpAddress.Invalidate() 'redraw this area

		'SHOW TIMING -----------------------------
		Dim TotTime As Integer = (CInt(RxBuff(5)) << 24) Or (CInt(RxBuff(6)) << 16) Or (CInt(RxBuff(7)) << 8) Or RxBuff(8)
		Dim TBS As Decimal = (CInt(RxBuff(9)) << 24) Or (CInt(RxBuff(10)) << 16) Or (CInt(RxBuff(11)) << 8) Or RxBuff(12)

		lbTotTime.Text = (TotTime / 1000).ToString("N2") & " mS"
		lbBitTime.Text = (TotTime / 32).ToString("N0") & " uS"
		lbSyncTime.Text = (TotTime / 32 * 8 / 1000).ToString("N2") & " mS"

		TBS = TBS / 1000    'convert to mS

		If TBS < 0 Then
			lbTBS.Text = "~ 0 mS"
		ElseIf TBS < 1000 Then  'less than 1 second
			lbTBS.Text = TBS.ToString("N2") & " mS"
		Else
			lbTBS.Text = (TBS / 1000).ToString("N2") & " Sec"
		End If

		If Learning = True Then
			lbOscClk.Text = (TotTime / 4096).ToString("N0") & " uS"
		Else
			lbOscClk.Text = (TotTime / 16384).ToString("N0") & " or " & (TotTime / 512).ToString("N0") & " uS"
		End If

	End Sub

	Private Sub grpAddr_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles grpAddress.Paint
		Dim CountTo As Integer = IIf(Learning = True, 9, IIf(rb6CH.Checked = True, 5, 7))    'count up to 5, 7 or 9, used on the FOR loops

		'drawing constants
		Const GrpCeroX As Integer = 5
		Const GrpCeroY As Integer = 15

		Dim CeroX As Integer = GrpCeroX + IIf(Learning = False, 20, 15)
		Const CeroY As Integer = GrpCeroY + 20
		Dim MaxX As Integer = grpAddress.Width - GrpCeroX - 5
		Dim MaxY As Integer = grpAddress.Height - GrpCeroY - 5

		Const SqWidth As Integer = 10
		Const SqHeight As Integer = 10
		Dim SepX As Integer = IIf(Learning = False, 5, 3)
		Const SepY As Integer = 5

		Dim LbHeight As Integer = lbAddr1.Height
		Dim LbWidth As Integer = lbAddr1.Width

		'drawing tools
		'Dim myGraphic As System.Drawing.Graphics = Me.grpAddress.CreateGraphics()
		Dim myGraphic As Graphics = e.Graphics
		Dim ClearBrush As New SolidBrush(Color.FromKnownColor(KnownColor.Control))
		Dim GrayBrush As New SolidBrush(Color.DarkGray)
		Dim RedBrush As New SolidBrush(Color.Red)

		'draw red box inside group, just to check the used space
		'myGraphic.DrawRectangle(Pens.Red, GrpCeroX, GrpCeroY, MaxX, MaxY)

		'clean groupbox
		'myGraphic.FillRectangle(ClearBrush, GrpCeroX, GrpCeroY, MaxX, MaxY)

		'position pin labels
		lbAddr9.Visible = Learning
		lbAddr10.Visible = Learning

		If chkInvPins.Checked = False Then
			lbAddr1.Location = New Point(CeroX + SqWidth * 0 + SepX * 0, CeroY - LbHeight)
			lbAddr2.Location = New Point(CeroX + SqWidth * 1 + SepX * 1, CeroY - LbHeight)
			lbAddr3.Location = New Point(CeroX + SqWidth * 2 + SepX * 2, CeroY - LbHeight)
			lbAddr4.Location = New Point(CeroX + SqWidth * 3 + SepX * 3, CeroY - LbHeight)
			lbAddr5.Location = New Point(CeroX + SqWidth * 4 + SepX * 4, CeroY - LbHeight)
			lbAddr6.Location = New Point(CeroX + SqWidth * 5 + SepX * 5, CeroY - LbHeight)
			lbAddr7.Location = New Point(CeroX + SqWidth * 6 + SepX * 6 + SqWidth / 2 - lbAddr7.Width / 2, CeroY - LbHeight)
			lbAddr8.Location = New Point(CeroX + SqWidth * 7 + SepX * 7 + SqWidth / 2 - lbAddr8.Width / 2, CeroY - LbHeight)
			If Learning = True Then
				lbAddr9.Location = New Point(CeroX + SqWidth * 8 + SepX * 8 + SqWidth / 2 - lbAddr9.Width / 2, CeroY - LbHeight)
				lbAddr10.Location = New Point(CeroX + SqWidth * 9 + SepX * 9 + SqWidth / 2 - lbAddr10.Width / 2, CeroY - LbHeight)
			End If

		Else
			If (Learning = True) Then
				lbAddr10.Location = New Point(CeroX + SqWidth * 0 + SepX * 0 + SqWidth / 2 - lbAddr10.Width / 2, CeroY - LbHeight)
				lbAddr9.Location = New Point(CeroX + SqWidth * 1 + SepX * 1 + SqWidth / 2 - lbAddr9.Width / 2, CeroY - LbHeight)
				lbAddr8.Location = New Point(CeroX + SqWidth * 2 + SepX * 2, CeroY - LbHeight)
				lbAddr7.Location = New Point(CeroX + SqWidth * 3 + SepX * 3, CeroY - LbHeight)
				lbAddr6.Location = New Point(CeroX + SqWidth * 4 + SepX * 4, CeroY - LbHeight)
				lbAddr5.Location = New Point(CeroX + SqWidth * 5 + SepX * 5, CeroY - LbHeight)
				lbAddr4.Location = New Point(CeroX + SqWidth * 6 + SepX * 6, CeroY - LbHeight)
				lbAddr3.Location = New Point(CeroX + SqWidth * 7 + SepX * 7, CeroY - LbHeight)
				lbAddr2.Location = New Point(CeroX + SqWidth * 8 + SepX * 8, CeroY - LbHeight)
				lbAddr1.Location = New Point(CeroX + SqWidth * 9 + SepX * 9, CeroY - LbHeight)
			Else
				lbAddr8.Location = New Point(CeroX + SqWidth * 0 + SepX * 0, CeroY - LbHeight)
				lbAddr7.Location = New Point(CeroX + SqWidth * 1 + SepX * 1, CeroY - LbHeight)
				lbAddr6.Location = New Point(CeroX + SqWidth * 2 + SepX * 2, CeroY - LbHeight)
				lbAddr5.Location = New Point(CeroX + SqWidth * 3 + SepX * 3, CeroY - LbHeight)
				lbAddr4.Location = New Point(CeroX + SqWidth * 4 + SepX * 4, CeroY - LbHeight)
				lbAddr3.Location = New Point(CeroX + SqWidth * 5 + SepX * 5, CeroY - LbHeight)
				lbAddr2.Location = New Point(CeroX + SqWidth * 6 + SepX * 6, CeroY - LbHeight)
				lbAddr1.Location = New Point(CeroX + SqWidth * 7 + SepX * 7, CeroY - LbHeight)
			End If

		End If

		'position H/L labels
		If chkInvHL.Checked = False Then
			lbAddrH.Location = New Point(CeroX - LbWidth - IIf(Learning = False, 5, 3), CeroY)
			lbAddrL.Location = New Point(CeroX - LbWidth - IIf(Learning = False, 5, 3), CeroY + SqHeight * 2 + SepY)
		Else
			lbAddrL.Location = New Point(CeroX - LbWidth - IIf(Learning = False, 5, 3), CeroY)
			lbAddrH.Location = New Point(CeroX - LbWidth - IIf(Learning = False, 5, 3), CeroY + SqHeight * 2 + SepY)
		End If

		'draw empty squares
		For x As Integer = 0 To CountTo Step 1
			myGraphic.DrawRectangle(Pens.Gray, CeroX + (x * SepX) + (x * SqWidth), CeroY, SqWidth - 1, SqHeight - 1)
			myGraphic.DrawRectangle(Pens.Gray, CeroX + (x * SepX) + (x * SqWidth), CeroY + SqHeight + SepY, SqWidth - 1, SqHeight - 1)
			myGraphic.DrawRectangle(Pens.Gray, CeroX + (x * SepX) + (x * SqWidth), CeroY + SqHeight + SepY + SqHeight + SepY, SqWidth - 1, SqHeight - 1)
		Next

		'draw filled squares
		Dim BitPair As Byte
		Dim PairCounter As Byte
		Dim WorkingPairCounter As Byte

		For PairCounter = 0 To CountTo Step 1
			'inverse pins checked
			If chkInvPins.Checked = True Then
				WorkingPairCounter = CountTo - PairCounter
			Else
				WorkingPairCounter = PairCounter
			End If

			'choose bit pair from appropiate byte
			If WorkingPairCounter < 4 Then
				BitPair = (AddressLo >> (WorkingPairCounter * 2)) And 3
			ElseIf WorkingPairCounter < 8 Then
				BitPair = (AddressMi >> (WorkingPairCounter - 4) * 2) And 3
			Else
				BitPair = (AddressHi >> (WorkingPairCounter - 8) * 2) And 3
			End If

			'inverse H/L if checked
			If chkInvHL.Checked = True Then
				If BitPair = 0 Then
					BitPair = 3
				ElseIf BitPair = 3 Then
					BitPair = 0
				End If
			End If

			Select Case BitPair
				Case 0  'bit pair is LOW (0b00)
					myGraphic.FillRectangle(GrayBrush, CeroX + (PairCounter * SepX) + (PairCounter * SqWidth), CeroY + SepY + SqHeight, SqWidth, SqHeight * 2 + SepY)
				Case 1  'bit pair is from a learning remote (0b01). Fixed code dont use this bit pair
					myGraphic.FillRectangle(RedBrush, CeroX + (PairCounter * SepX) + (PairCounter * SqWidth), CeroY + SepY + SqHeight, SqWidth, SqHeight)
				Case 2  'bit pair is FLOAT (010)
					myGraphic.FillRectangle(GrayBrush, CeroX + (PairCounter * SepX) + (PairCounter * SqWidth), CeroY + SepY + SqHeight, SqWidth, SqHeight)
				Case 3  'bit pair is HIGH (0b11)
					myGraphic.FillRectangle(GrayBrush, CeroX + (PairCounter * SepX) + (PairCounter * SqWidth), CeroY, SqWidth, SqHeight * 2 + SepY)
			End Select
		Next

		'remove pins 7 and 8 from address drawing
		If rb6CH.Checked = True Then
			lbAddr7.Visible = False
			lbAddr8.Visible = False
		Else
			lbAddr7.Visible = True
			lbAddr8.Visible = True
		End If

		myGraphic.Dispose()
	End Sub

	Private Sub chkInvHL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInvHL.CheckedChanged
		grpAddress.Invalidate()
	End Sub

	Private Sub chkInvPins_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInvPins.CheckedChanged
		grpAddress.Invalidate()
	End Sub

	Private Sub rbChannels_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb4CH.CheckedChanged, rb6CH.CheckedChanged, rb8CH.CheckedChanged
		If sender.Checked = True Then
			If rb4CH.Checked = True Then
				'invisible receiving buttons D4-D7
				btnD4.Visible = False
				btnD5.Visible = False
				btnD6.Visible = False
				btnD7.Visible = False

				'invisible sending buttons D4-D7
				chkD4.Visible = False
				chkD5.Visible = False
				chkD6.Visible = False
				chkD7.Visible = False

				'enable address bits 7 and 8
				pnl7.Enabled = True
				pnl8.Enabled = True

				'enable visual send learning checkbox
				chkLearning.Enabled = True

			ElseIf rb6CH.Checked = True Then
				'visible receiving buttons
				btnD4.Visible = True
				btnD5.Visible = True
				btnD6.Visible = False
				btnD7.Visible = False

				'visible sending buttons
				chkD4.Visible = True
				chkD5.Visible = True
				chkD6.Visible = False
				chkD7.Visible = False

				'disable address bits 7 and 8
				pnl7.Enabled = False
				pnl8.Enabled = False

				'disable visual send learning checkbox
				chkLearning.Enabled = False
				chkLearning.Checked = False

			ElseIf rb8CH.Checked = True Then
				'visible receiving buttons
				btnD4.Visible = True
				btnD5.Visible = True
				btnD6.Visible = True
				btnD7.Visible = True

				'visible sending buttons
				chkD4.Visible = True
				chkD5.Visible = True
				chkD6.Visible = True
				chkD7.Visible = True

				'enable address bits 7 and 8
				pnl7.Enabled = True
				pnl8.Enabled = True

				'disable visual send learning checkbox
				chkLearning.Enabled = False
				chkLearning.Checked = False
			End If

			ThreeStateButtons()
			chk8CHinv.Enabled = rb8CH.Checked

			DecodeData()
			GetVisualValue()
		End If

	End Sub

	Private Sub chk8CHinv_Click(sender As Object, e As EventArgs) Handles chk8CHinv.Click
		DecodeData()
		GetVisualValue()
	End Sub

	Private Sub txtHexSend_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtHexSend.KeyPress
		'only allow hex characters
		If (e.KeyChar >= "0") And (e.KeyChar <= "9") Then
			'allowed, nothing to do
		ElseIf (e.KeyChar >= "a") And (e.KeyChar <= "f") Then
			e.KeyChar = Char.ToUpper(e.KeyChar) 'convert to upper case
		ElseIf (e.KeyChar >= "A") And (e.KeyChar <= "F") Then
			'allowed, nothing to do
		ElseIf e.KeyChar = Convert.ToChar(Keys.Back) Then
			'allowed, nothing to do
		Else
			e.Handled = True
		End If
	End Sub

	Private Sub txtHexSend_LostFocus(sender As Object, e As EventArgs) Handles txtHexSend.LostFocus
		txtHexSend.Text = txtHexSend.Text.Replace(" ", "")  'remove spaces

		Dim AddZeros As Byte = 6 - txtHexSend.Text.Length

		Select Case AddZeros
			Case 1
				txtHexSend.Text = txtHexSend.Text.Insert(0, "0")
			Case 2
				txtHexSend.Text = txtHexSend.Text.Insert(0, "00")
			Case 3
				txtHexSend.Text = txtHexSend.Text.Insert(0, "000")
			Case 4
				txtHexSend.Text = txtHexSend.Text.Insert(0, "0000")
			Case 5
				txtHexSend.Text = txtHexSend.Text.Insert(0, "00000")
			Case 6
				txtHexSend.Text = txtHexSend.Text.Insert(0, "000000")

		End Select

		txtHexSend.MaxLength = 8
		txtHexSend.Text = txtHexSend.Text.Insert(2, " ")
		txtHexSend.Text = txtHexSend.Text.Insert(5, " ")
	End Sub

	Private Sub txtHexSend_GotFocus(sender As Object, e As EventArgs) Handles txtHexSend.GotFocus
		txtHexSend.Text = txtHexSend.Text.Replace(" ", "")  'remove spaces
		txtHexSend.MaxLength = 6
	End Sub

	Private Sub btnHexClear_Click(sender As Object, e As EventArgs) Handles btnHexClear.Click
		txtHexSend.Text = ""
		txtHexSend.Focus()
	End Sub

	Private Sub btnHexSend_Click(sender As Object, e As EventArgs) Handles btnHexSend.Click
		Dim s As String = txtHexSend.Text

		'remove any spaces from, e.g. "A0 20 34 34"
		s = s.Replace(" "c, "")

		'make sure we have an even number of digits
		If (s.Length And 1) = 1 Then
			Throw New FormatException("Odd string length when even string length is required.")
		End If

		'calculate the length of the byte array and dim an array to that
		Dim nBytes = s.Length \ 2

		'pick out every two bytes and convert them from hex representation
		For i = 0 To nBytes - 1
			HexDataTx(i) = Convert.ToByte(s.Substring(i * 2, 2), 16)
		Next

		'Debug.Print(Hex(HexDataTx))
		TxSend(HexDataTx)
	End Sub

	Private Sub chkLearning_CheckedChanged(sender As Object, e As EventArgs) Handles chkLearning.CheckedChanged
		'enable or disable the learning floating option (red dot) in the radio buttons
		rbA1Fl.Enabled = chkLearning.Checked
		rbA2Fl.Enabled = chkLearning.Checked
		rbA3Fl.Enabled = chkLearning.Checked
		rbA4Fl.Enabled = chkLearning.Checked
		rbA5Fl.Enabled = chkLearning.Checked
		rbA6Fl.Enabled = chkLearning.Checked
		rbA7Fl.Enabled = chkLearning.Checked
		rbA8Fl.Enabled = chkLearning.Checked

		'enable or disable the 9 and 10th address possitions
		pnl9.Enabled = chkLearning.Checked
		pnl10.Enabled = chkLearning.Checked

		'if any of the radio buttons is in the red dot possion, and learning is unchecked, then change rb to fixed learning option
		If chkLearning.Checked = False Then
			If rbA1Fl.Checked = True Then
				rbA1F.Checked = True
			End If
			If rbA2Fl.Checked = True Then
				rbA2F.Checked = True
			End If
			If rbA3Fl.Checked = True Then
				rbA3F.Checked = True
			End If
			If rbA4Fl.Checked = True Then
				rbA4F.Checked = True
			End If
			If rbA5Fl.Checked = True Then
				rbA5F.Checked = True
			End If
			If rbA6Fl.Checked = True Then
				rbA6F.Checked = True
			End If
			If rbA7Fl.Checked = True Then
				rbA7F.Checked = True
			End If
			If rbA8Fl.Checked = True Then
				rbA8F.Checked = True
			End If
		End If

		ThreeStateButtons() 'enable or disable tristate

		GetVisualValue()    'show hex value
	End Sub

	Private Sub GetVisualValue()
		Dim tmp As Byte

		VisualDataTx(0) = 0
		VisualDataTx(1) = 0
		VisualDataTx(2) = 0

		'A0
		If rbA1L.Checked = True Then        'low
			tmp = LOW
		ElseIf rbA1Fl.Checked = True Then   'learning float
			tmp = LEARNING_FLOAT
		ElseIf rbA1F.Checked = True Then    'float
			tmp = FIXED_FLOAT
		ElseIf rbA1H.Checked = True Then    'high
			tmp = HIGH
		End If

		VisualDataTx(2) = tmp

		'A1
		If rbA2L.Checked = True Then        'low
			tmp = LOW
		ElseIf rbA2Fl.Checked = True Then   'learning float
			tmp = LEARNING_FLOAT
		ElseIf rbA2F.Checked = True Then    'float
			tmp = FIXED_FLOAT
		ElseIf rbA2H.Checked = True Then    'high
			tmp = HIGH
		End If

		VisualDataTx(2) = (tmp << 2) Or VisualDataTx(2)

		'A2
		If rbA3L.Checked = True Then        'low
			tmp = LOW
		ElseIf rbA3Fl.Checked = True Then   'learning float
			tmp = LEARNING_FLOAT
		ElseIf rbA3F.Checked = True Then    'float
			tmp = FIXED_FLOAT
		ElseIf rbA3H.Checked = True Then    'high
			tmp = HIGH
		End If

		VisualDataTx(2) = (tmp << 4) Or VisualDataTx(2)

		'A3
		If rbA4L.Checked = True Then        'low
			tmp = LOW
		ElseIf rbA4Fl.Checked = True Then   'learning float
			tmp = LEARNING_FLOAT
		ElseIf rbA4F.Checked = True Then    'float
			tmp = FIXED_FLOAT
		ElseIf rbA4H.Checked = True Then    'high
			tmp = HIGH
		End If

		VisualDataTx(2) = (tmp << 6) Or VisualDataTx(2)

		'A4
		If rbA5L.Checked = True Then        'low
			tmp = LOW
		ElseIf rbA5Fl.Checked = True Then   'learning float
			tmp = LEARNING_FLOAT
		ElseIf rbA5F.Checked = True Then    'float
			tmp = FIXED_FLOAT
		ElseIf rbA5H.Checked = True Then    'high
			tmp = HIGH
		End If

		VisualDataTx(1) = tmp Or VisualDataTx(1)

		'A5
		If rbA6L.Checked = True Then        'low
			tmp = LOW
		ElseIf rbA6Fl.Checked = True Then   'learning float
			tmp = LEARNING_FLOAT
		ElseIf rbA6F.Checked = True Then    'float
			tmp = FIXED_FLOAT
		ElseIf rbA6H.Checked = True Then    'high
			tmp = HIGH
		End If

		VisualDataTx(1) = (tmp << 2) Or VisualDataTx(1)

		'for 6ch we dont need A6 and A7
		If rb6CH.Checked = False Then
			'A6
			If rbA7L.Checked = True Then        'low
				tmp = LOW
			ElseIf rbA7Fl.Checked = True Then   'learning float
				tmp = LEARNING_FLOAT
			ElseIf rbA7F.Checked = True Then    'float
				tmp = FIXED_FLOAT
			ElseIf rbA7H.Checked = True Then    'high
				tmp = HIGH
			End If

			VisualDataTx(1) = (tmp << 4) Or VisualDataTx(1)

			'A7
			If rbA8L.Checked = True Then        'low
				tmp = LOW
			ElseIf rbA8Fl.Checked = True Then   'learning float
				tmp = LEARNING_FLOAT
			ElseIf rbA8F.Checked = True Then    'float
				tmp = FIXED_FLOAT
			ElseIf rbA8H.Checked = True Then    'high
				tmp = HIGH
			End If

			VisualDataTx(1) = (tmp << 6) Or VisualDataTx(1)

		Else    'for 6ch we use D4 and D5
			'D4
			If chkD4.CheckState = CheckState.Checked Then
				tmp = HIGH
			ElseIf chkD4.CheckState = CheckState.Indeterminate Then
				tmp = FIXED_FLOAT
			ElseIf chkD4.CheckState = CheckState.Unchecked Then
				tmp = LOW
			End If

			VisualDataTx(1) = tmp << 6

			'D5
			If chkD5.CheckState = CheckState.Checked Then
				tmp = HIGH
			ElseIf chkD5.CheckState = CheckState.Indeterminate Then
				tmp = FIXED_FLOAT
			ElseIf chkD5.CheckState = CheckState.Unchecked Then
				tmp = LOW
			End If

			VisualDataTx(1) = (tmp << 4) Or VisualDataTx(1)
		End If

		'learning remote has A8 and A9 address
		If chkLearning.Checked = True Then
			'A8
			If rbA9L.Checked = True Then        'low
				tmp = LOW
			ElseIf rbA9Fl.Checked = True Then   'learning float
				tmp = LEARNING_FLOAT
			ElseIf rbA9F.Checked = True Then    'float
				tmp = FIXED_FLOAT
			ElseIf rbA9H.Checked = True Then    'high
				tmp = HIGH
			End If

			VisualDataTx(0) = tmp Or VisualDataTx(0)

			'A9
			If rbA10L.Checked = True Then       'low
				tmp = LOW
			ElseIf rbA10Fl.Checked = True Then   'learning float
				tmp = LEARNING_FLOAT
			ElseIf rbA10F.Checked = True Then   'float
				tmp = FIXED_FLOAT
			ElseIf rbA10H.Checked = True Then   'high
				tmp = HIGH
			End If

			VisualDataTx(0) = (tmp << 2) Or VisualDataTx(0)

			'buttons
			tmp = IIf(chkD0.Checked = True, 1, 0)       'D0
			tmp = tmp + IIf(chkD1.Checked = True, 2, 0) 'D1
			tmp = tmp + IIf(chkD2.Checked = True, 4, 0) 'D2
			tmp = tmp + IIf(chkD3.Checked = True, 8, 0) 'D3

			VisualDataTx(0) = (tmp << 4) Or VisualDataTx(0)
		Else
			'4ch and 6ch buttons (not learning)
			If rb8CH.Checked = False Then
				'D0
				If chkD0.CheckState = CheckState.Checked Then
					tmp = HIGH
				ElseIf chkD0.CheckState = CheckState.Indeterminate Then
					tmp = FIXED_FLOAT
				ElseIf chkD0.CheckState = CheckState.Unchecked Then
					tmp = LOW
				End If

				VisualDataTx(0) = tmp << 6

				'D1
				If chkD1.CheckState = CheckState.Checked Then
					tmp = HIGH
				ElseIf chkD1.CheckState = CheckState.Indeterminate Then
					tmp = FIXED_FLOAT
				ElseIf chkD1.CheckState = CheckState.Unchecked Then
					tmp = LOW
				End If

				VisualDataTx(0) = (tmp << 4) Or VisualDataTx(0)

				'D2
				If chkD2.CheckState = CheckState.Checked Then
					tmp = HIGH
				ElseIf chkD2.CheckState = CheckState.Indeterminate Then
					tmp = FIXED_FLOAT
				ElseIf chkD2.CheckState = CheckState.Unchecked Then
					tmp = LOW
				End If

				VisualDataTx(0) = (tmp << 2) Or VisualDataTx(0)

				'D3
				If chkD3.CheckState = CheckState.Checked Then
					tmp = HIGH
				ElseIf chkD3.CheckState = CheckState.Indeterminate Then
					tmp = FIXED_FLOAT
				ElseIf chkD3.CheckState = CheckState.Unchecked Then
					tmp = LOW
				End If

				VisualDataTx(0) = tmp Or VisualDataTx(0)

			Else
				'non inverted 8ch
				If chk8CHinv.Checked = False Then
					tmp = IIf(chkD7.Checked = True, &B11, 0)            'D7
					tmp = IIf(chkD6.Checked = True, &B1100, tmp)        'D6
					tmp = IIf(chkD5.Checked = True, &B1111, tmp)        'D5
					tmp = IIf(chkD4.Checked = True, &B110000, tmp)      'D4
					tmp = IIf(chkD3.Checked = True, &B110011, tmp)      'D3
					tmp = IIf(chkD2.Checked = True, &B111100, tmp)      'D2
					tmp = IIf(chkD1.Checked = True, &B111111, tmp)      'D1
					tmp = IIf(chkD0.Checked = True, &B11000000, tmp)    'D0
				Else    'inverted 8ch
					tmp = IIf(chkD0.Checked = True, &B11, 0)            'D0
					tmp = IIf(chkD1.Checked = True, &B1100, tmp)        'D1
					tmp = IIf(chkD2.Checked = True, &B1111, tmp)        'D2
					tmp = IIf(chkD3.Checked = True, &B110000, tmp)      'D3
					tmp = IIf(chkD4.Checked = True, &B110011, tmp)      'D4
					tmp = IIf(chkD5.Checked = True, &B111100, tmp)      'D5
					tmp = IIf(chkD6.Checked = True, &B111111, tmp)      'D6
					tmp = IIf(chkD7.Checked = True, &B11000000, tmp)    'D7
				End If

				VisualDataTx(0) = tmp
			End If
		End If

		lbHexVisualSend.Text = "0x " & Hex(VisualDataTx(0)).PadLeft(2, "0") & " " & Hex(VisualDataTx(1)).PadLeft(2, "0") & " " & Hex(VisualDataTx(2)).PadLeft(2, "0")

	End Sub

	Private Sub btnVisualSend_Click(sender As Object, e As EventArgs) Handles btnVisualSend.Click
		'Debug.Print(Hex(VisualDataTx))
		TxSend(VisualDataTx)
	End Sub

	Private Sub chkButtons_Click(sender As Object, e As EventArgs) Handles chkD0.Click, chkD1.Click, chkD2.Click, chkD3.Click, chkD4.Click, chkD5.Click, chkD6.Click, chkD7.Click
		'8ch only allow one button at a time
		If rb8CH.Checked = True Then
			Dim state As Boolean = sender.checked   'save new state

			'uncheck all buttons
			chkD0.Checked = False
			chkD1.Checked = False
			chkD2.Checked = False
			chkD3.Checked = False
			chkD4.Checked = False
			chkD5.Checked = False
			chkD6.Checked = False
			chkD7.Checked = False

			sender.checked = state  'restore state
		End If

		If sender.CheckState = CheckState.Checked Then
			sender.FlatAppearance.CheckedBackColor = Color.Green
		ElseIf sender.Checkstate = CheckState.Indeterminate Then
			sender.FlatAppearance.CheckedBackColor = Color.Gray
		End If

		GetVisualValue()    'show hex value
	End Sub

	Private Sub rbAddr(sender As Object, e As EventArgs) Handles rbA1H.Click, rbA2H.Click, rbA3H.Click, rbA4H.Click, rbA5H.Click, rbA6H.Click, rbA7H.Click, rbA8H.Click, rbA9H.Click, rbA10H.Click,
																	rbA1F.Click, rbA2F.Click, rbA3F.Click, rbA4F.Click, rbA5F.Click, rbA6F.Click, rbA7F.Click, rbA8F.Click, rbA9F.Click, rbA10F.Click,
																	rbA1Fl.Click, rbA2Fl.Click, rbA3Fl.Click, rbA4Fl.Click, rbA5Fl.Click, rbA6Fl.Click, rbA7Fl.Click, rbA8Fl.Click, rbA9Fl.Click, rbA10Fl.Click,
																	rbA1L.Click, rbA2L.Click, rbA3L.Click, rbA4L.Click, rbA5L.Click, rbA6L.Click, rbA7L.Click, rbA8L.Click, rbA9L.Click, rbA10L.Click


		GetVisualValue()    'show hex value
	End Sub

	Private Sub ThreeStateButtons()
		'enable or disable tristate
		If rb8CH.Checked = False And chkLearning.Checked = False Then
			chkD0.ThreeState = True
			chkD1.ThreeState = True
			chkD2.ThreeState = True
			chkD3.ThreeState = True
			chkD4.ThreeState = True
			chkD5.ThreeState = True

		Else
			chkD0.ThreeState = False
			chkD1.ThreeState = False
			chkD2.ThreeState = False
			chkD3.ThreeState = False
			chkD4.ThreeState = False
			chkD5.ThreeState = False

			chkD0.CheckState = IIf(chkD0.CheckState = CheckState.Indeterminate, CheckState.Unchecked, chkD0.CheckState)
			chkD1.CheckState = IIf(chkD1.CheckState = CheckState.Indeterminate, CheckState.Unchecked, chkD1.CheckState)
			chkD2.CheckState = IIf(chkD2.CheckState = CheckState.Indeterminate, CheckState.Unchecked, chkD2.CheckState)
			chkD3.CheckState = IIf(chkD3.CheckState = CheckState.Indeterminate, CheckState.Unchecked, chkD3.CheckState)
			chkD4.CheckState = IIf(chkD4.CheckState = CheckState.Indeterminate, CheckState.Unchecked, chkD4.CheckState)
			chkD5.CheckState = IIf(chkD5.CheckState = CheckState.Indeterminate, CheckState.Unchecked, chkD5.CheckState)

		End If

	End Sub

	Private Sub frmMain_HelpButtonClicked(sender As Object, e As CancelEventArgs) Handles Me.HelpButtonClicked
		frmHelp.ShowDialog()
		e.Cancel = True
	End Sub
End Class



