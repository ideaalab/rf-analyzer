﻿Option Strict On

Module Module1
	Public Const HEADER1 As Byte = &HFF     'header of TX and RX
	Public Const HEADER2 As Byte = &H55

	Public Const HIGH As Byte = &B11
	Public Const FIXED_FLOAT As Byte = &B10
	Public Const LEARNING_FLOAT As Byte = &B1
	Public Const LOW As Byte = &B0

	Public Const RX_BUFF_SIZE As Byte = 13  'RX buffer size
	Public Const TX_BUFF_SIZE As Byte = 7   'TX buffer size

	Public RxBuff(RX_BUFF_SIZE) As Byte     'RX buffer
	Public TxBuff(TX_BUFF_SIZE) As Byte     'TX buffer

	Public HexDataTx(3) As Byte             'store the value of hex send
	Public VisualDataTx(3) As Byte          'store the value of visual send

	Public Sub TxSend(ByVal TxArray() As Byte)
		Dim Sent As String
		Dim PulsLen As Byte

		'choose PulseLenght according to user selection
		If frmMain.rd16mS.Checked = True Then
			PulsLen = 16
		ElseIf frmmain.rd32mS.Checked = True Then
			PulsLen = 32
		ElseIf frmMain.rd48mS.Checked = True Then
			PulsLen = 48
		ElseIf frmMain.rd64mS.Checked = True Then
			PulsLen = 64
		End If

		'header
		TxBuff(0) = HEADER1         'place header on the first...
		TxBuff(1) = HEADER2         '...two possitions

		'data
		TxBuff(2) = TxArray(0)      'low
		TxBuff(3) = TxArray(1)      'mid
		TxBuff(4) = TxArray(2)      'high

		'pulse lenght and number
		TxBuff(5) = PulsLen                                     'store the pulse length
		TxBuff(6) = Convert.ToByte(frmMain.txtNumPulses.Value)  'store the number of pulses

		frmMain.Com.Write(TxBuff, 0, TX_BUFF_SIZE)  'send trough COM

		'------ print on textbox -----------
		Sent = "Sent: "

		For x As Integer = 0 To (TX_BUFF_SIZE - 1) Step 1
			Sent = Sent & "0x" & Hex(TxBuff(x)).PadLeft(2, "0"c) & " "
		Next

		frmMain.txtComm.Text = Sent & vbCrLf & vbCrLf & frmMain.txtComm.Text
		'-----------------------------------


	End Sub
End Module
